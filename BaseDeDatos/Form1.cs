﻿using BaseDeDatos.Analizadores;
using Irony.Parsing;
using System;
using System.Windows.Forms;
using System.Net;
using BaseDeDatos.Ast;
using BaseDeDatos.Ast.SSL;
using BaseDeDatos.Graficar;

namespace BaseDeDatos
{
    public partial class Form1 : Form
    {
        IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
        String salida = "";
        public int contador_break = 0;

        public Form1()
        {
            InitializeComponent();
           // AsynchronousSocketListener.StartListening();
        }

        public void appendSalida(string cadena)
        {
            salida += cadena + "\n";
            //textConsola.AppendText(cadena);
           // Console.WriteLine(cadena + "\n");
        }

        private void btnAnalizar_Click(object sender, EventArgs e)
        {
            salida = "";
            textConsola.Text = "";
            GramaticaSql gram = new GramaticaSql();
           // GramaticaP gram = new GramaticaP();
            LanguageData lenguaje = new LanguageData(gram);
            Parser parser = new Parser(lenguaje);

            try
            {
                ParseTree arbol = parser.Parse(textEntrada.Text);
                ParseTreeNode raiz = arbol.Root;

                if (raiz == null || arbol.ParserMessages.Count > 0 || arbol.HasErrors())
                {
                    // consolaTxt.Text += "Error sintactico en la cadena de entrada\r\n";
                    foreach (var item in arbol.ParserMessages)
                    {
                        textConsola.Text += "Linea:" + (Convert.ToInt32(item.Location.Line) + 1).ToString() + ", Colum:" + item.Location.Column + ", Msg:" + item.Message + "\r\n";
                    }
                }
                else
                {
                    generarImagen(raiz);
                    textConsola.Text += "Cadena de entrada sintacticamente correcta \n";
                 //   InterpreteP an = new InterpreteP();
                    
                    InterpreteSql an = new InterpreteSql();
                    an.Analizar(arbol.Root);
                    AST auxArbol = an.Analizar(arbol.Root);
                    Entorno global = new Entorno(null);
                    try
                    {
                        if (auxArbol != null)
                        {
                            foreach (Instruccion ins in auxArbol.Instrucciones)
                            {
                                if (ins is Funcion)
                                {
                                    Funcion funcion = (Funcion)ins;
                                    global.agregar(funcion.Identificador, funcion);
                 
                                }
                                else if (ins is Declaracion)
                                {
                                    Declaracion declaracion = (Declaracion)ins;
                                    declaracion.ejecutar(global, auxArbol);
                                }
                                else {
                                    ((Instruccion)ins).ejecutar(global, auxArbol);
                                }
                            }
                         
                        }
                        else
                        {
                            MessageBox.Show("El arbol de Irony no se construyo. Cadena invalida!\n");
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());

                    }
                }

                textConsola.Text += salida + "\n";
            }           
            catch (Exception eX)
            {
                MessageBox.Show(eX.ToString());
            }
        }

        private static void generarImagen(ParseTreeNode raiz)
        {
            String graf = graficar.getdot(raiz);
            WINGRAPHVIZLib.DOT dot = new WINGRAPHVIZLib.DOT();
            WINGRAPHVIZLib.BinaryImage img = dot.ToPNG(graf);
            img.Save("AST.png");
        }

        private void btnSendInfoSockets_Click(object sender, EventArgs e)
        {
        }
    }



}
