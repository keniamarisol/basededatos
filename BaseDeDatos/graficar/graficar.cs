﻿using Irony.Parsing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseDeDatos.Graficar
{
    class graficar
    {

        private static int contador = 1;
        public static String grafo;

        public static String getdot(ParseTreeNode raiz)
        {
            grafo = "\ndigraph G {\r\nnode [shape=record, style=filled, color=pink, fontcolor=black];\n";
            grafo += "nodo0[label= \"" + escapar(raiz.ToString()) + "\"]\n";
            recorrerAST("nodo0", raiz);
            grafo += "}";
            return grafo;
        }

        private static void recorrerAST(String padre, ParseTreeNode hijos)
        {
            foreach (ParseTreeNode hijo in hijos.ChildNodes)
            {
                String nombreHijo = "nodo" + contador.ToString();
                grafo += nombreHijo + "[label=\"" + escapar(hijo.ToString()) + "\"]\n";
                grafo += padre + "->" + nombreHijo + ";\n";
                contador++;
                recorrerAST(nombreHijo, hijo);
            }
        }

        private static String escapar(String cadena)
        {
            cadena = cadena.Replace("\\", "\\\\");
            cadena = cadena.Replace("\"", "\\\"");
            cadena = cadena.Replace(">", "mayor");
            cadena = cadena.Replace("<", "menor");
            cadena = cadena.Replace("\n", " ");
            return cadena;
        }
    }
}
