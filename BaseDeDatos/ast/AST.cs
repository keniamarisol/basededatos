﻿using BaseDeDatos.Ast.SSL;
using System.Collections.Generic;

namespace BaseDeDatos.Ast
{
    public class AST
    {
       // private LinkedList<Struct> structs;
        private LinkedList<Instruccion> instrucciones;
        private LinkedList<NodoAST> v;

        public AST(LinkedList<Instruccion> instrucciones)
        {
          //  Structs = new LinkedList<Struct>();
            this.Instrucciones = instrucciones;
        }

     

        /*  public void agregarStruct(Struct s)
          {
              Structs.AddLast(s);
          }


          public LinkedList<Struct> Structs
          {
              get
              {
                  return structs;
              }

              set
              {
                  structs = value;
              }
          }*/

        public LinkedList<Instruccion> Instrucciones
        {
            get
            {
                return instrucciones;
            }

            set
            {
                instrucciones = value;
            }
        }

    }

}

