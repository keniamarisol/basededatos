﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseDeDatos.SistemaDeArchivos
{
    class ManejarArchivo
    {


        public String leer(String ruta)
        {
            string readContents;
            try
            {

                using (StreamReader streamReader = new StreamReader(ruta, Encoding.UTF8))
                {
                    readContents = streamReader.ReadToEnd();
                }
                return readContents;
            }
            catch (Exception e)
            {
                Program.getGUI().appendSalida("ERROR, no se puede leer el archivo.");
                return null;
            }

        }

        public void escribir(String nombre, String texto)
        {
            try
            {
                System.IO.File.WriteAllText(nombre, texto);
            }
            catch (IOException e)
            {
                Program.getGUI().appendSalida("ERROR, no se puede escribir el archivo.");

            }

        }

        public void modificar(String ruta, String texto)
        {

            try
            {
                System.IO.File.AppendAllText(ruta, texto);
            }
            catch (IOException e)
            {
                Program.getGUI().appendSalida("ERROR, no se puede modificar el archivo.");

            }

        }

        public void CrearCarpeta(String ruta, String name)
        {

            string folderName = ruta;
            string pathString = System.IO.Path.Combine(ruta, name);
            try
            {
                if (!Directory.Exists(pathString))
                {
                    Directory.CreateDirectory(pathString);
                    // return true;
                }
                else
                {
                    Console.WriteLine("File \"{0}\" Ya existe.", "hola_mundo");
                    Program.getGUI().appendSalida("File \"{0}\" Ya existe.");
                    Program.getGUI().appendSalida("hola_mundo");
                    // return false;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Program.getGUI().appendSalida(ex.Message);

            }
            //  return false;
        }


    }
}
