﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseDeDatos.Ast;
using static BaseDeDatos.Ast.Simbolo;

namespace BaseDeDatos.SistemaDeArchivos
{
    public class Campo
    {
        String nombre, tforanea, objeto;
        Tipos tipo;
        bool nulo, autoincrementable, primaria, foranea, unica;


        public Tipos Tipo { get => Tipo1; set => Tipo1 = value; }
        public string Nombre { get => Nombre1; set => Nombre1 = value; }
        public string Tforanea { get => tforanea; set => tforanea = value; }
        public string Nombre1 { get => nombre; set => nombre = value; }
        public Tipos Tipo1 { get => tipo; set => tipo = value; }
        public bool Nulo { get => nulo; set => nulo = value; }
        public bool Autoincrementable { get => autoincrementable; set => autoincrementable = value; }
        public bool Primaria { get => primaria; set => primaria = value; }
        public bool Foranea { get => foranea; set => foranea = value; }
        public bool Unica { get => unica; set => unica = value; }

        public Campo(String nombre, Tipos tipo)
        {
            this.Nombre = nombre;
            this.Tipo = tipo;
            this.Nulo = this.Autoincrementable = this.Primaria = this.Foranea = this.Unica = false;
        }

        public Campo(string nombre, string tforanea, string objeto, Tipos tipo, bool nulo, bool autoincrementable, bool primaria, bool foranea, bool unica)
        {
            this.Nombre = nombre;
            this.Tforanea = tforanea;
            this.objeto = objeto;
            this.Tipo = tipo;
            this.Nulo = nulo;
            this.Autoincrementable = autoincrementable;
            this.Primaria = primaria;
            this.Foranea = foranea;
            this.Unica = unica;
        }

        public void setAutoincrementable(bool autoincrementable)
        {
            this.Autoincrementable = autoincrementable;
        }

        public void setPrimaria(bool primaria)
        {
            this.Primaria = primaria;
            this.Nulo = false;
            this.Unica = true;
        }

        public void setForanea(String tabla)
        {
            this.Foranea = true;
            this.Tforanea = tabla;
        }

        public void setNulo(bool nulo)
        {
            this.Nulo = nulo;
        }

        public void setUnica(bool unica)
        {
            this.Unica = unica;
        }


        public bool isNulo()
        {
            return Nulo;
        }

        public bool isAutoincrementable()
        {
            return Autoincrementable;
        }

        public bool isPrimaria()
        {
            return Primaria;
        }

        public bool isForanea()
        {
            return Foranea;
        }

        public bool isUnica()
        {
            return Unica;
        }

        public String getComplementos()
        {
            String complementos = "";
            if (Nulo)
            {
                complementos += "nulo ";
            }
            if (Autoincrementable)
            {
                complementos += "auto ";
            }
            if (Primaria)
            {
                complementos += "primaria ";
            }
            if (Unica)
            {
                complementos += "unica ";
            }
            return complementos;
        }
    }
}
