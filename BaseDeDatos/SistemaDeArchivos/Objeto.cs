﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static BaseDeDatos.Ast.Simbolo;

namespace BaseDeDatos.SistemaDeArchivos
{
    public class Objeto
    {
        public Tipos tipo;
        public int numero;
        public double dec;
        public bool boolean;
        public String texto;
        protected DateTime fecha;

        // Atributos para el manejo de objetos!
        String nombre;
        List<Objeto> atributos;

        public String tabla = "";

        // Constructor para numeros enteros
        public Objeto(int numero)
        {
            this.numero = numero;
            this.texto = numero.ToString();
            this.tipo = Tipos.INT;
        }

        // Constructor para numeros deces
        public Objeto(double dec)
        {
            this.dec = dec;
            this.texto = dec.ToString();
            this.tipo = Tipos.DOUBLE;
        }

        // Constructor para boolos
        public Objeto(bool boolean)
        {
            this.boolean = boolean;
            if (boolean)
            {
                this.texto = "true";
            }
            else
            {
                this.texto = "false";
            }
            this.tipo = Tipos.BOOL;
        }

        // Constructor para cadenas
        public Objeto(String texto)
        {
            this.texto = texto;
            this.tipo = Tipos.STRING;
        }

        // Constructor para fechas (date,datetime)
        public Objeto(Tipos tipo, String fecha)
        {
            this.texto = fecha;
            if (tipo == Tipos.DATE)
            {
            }
            else
            {
            }
            try
            {
                
            }
            catch (Exception e)
            {
                Program.getGUI().appendSalida("ERROR, no se puede convertir a Datetime: " + fecha);
            }
            this.tipo = tipo;
        }

        // Obtener fecha
        public String getFecha()
        {
            if (tipo == Tipos.DATE)
            {
            }
            else
            {
            }
            return fecha.ToString();
        }

        // Constructor para objetos
        public Objeto(String nombre, Tipos tipo)
        {
            this.nombre = nombre;
            this.tipo = tipo;
            this.atributos = new List<Objeto>();
        }

        // Agregar un atributo al objeto
        public void agregarAtributo(Objeto atr)
        {
            atributos.Add(atr);
        }

        // Colocar un nombre al objeto
        public void setNombre(String n)
        {
            this.nombre = n;
        }

        // Obtener el nombre del objeto
        public String getNombre()
        {
            return nombre;
        }

        public List<Objeto> getAtributos()
        {
            return atributos;
        }

        public Tipos getTipo()
        {
            return tipo;
        }

        public void modificarAtributo(String nombre, Objeto objeto)
        {
            objeto.setNombre(nombre);
            int i = 0;
            foreach (Objeto atr in atributos)
            {
                i++;
                if (atr.nombre.Equals(nombre))
                {
                   //atributos.Remove(atr);
                    atributos[i] = objeto; 
                    break;
                }
            }
        }

    }
}
