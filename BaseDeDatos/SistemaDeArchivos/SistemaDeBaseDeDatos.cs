﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static BaseDeDatos.Ast.Simbolo;

namespace BaseDeDatos.SistemaDeArchivos
{
    public class SistemaDeBaseDeDatos
    {

        private static SistemaDeBaseDeDatos ourInstance = new SistemaDeBaseDeDatos();
        public static String dbActual = "";
        public static String usuario = "";
        public static String rutaMaestro = "C:\\Users\\Kenia\\Documents\\COMPI2\\proyecto1\\SistemaDeArchivos\\Maestro.xml";

 

        public static String rutaBds = "C:\\Users\\Kenia\\Documents\\COMPI2\\proyecto1\\SistemaDeArchivos\\";
        public static String rutaBackup = "C:\\Users\\Kenia\\Documents\\COMPI2\\proyecto1\\SistemaDeArchivos\\";
      //  public LinkedList<Usuario> usuarios;
        public static Hashtable basesDatos = new Hashtable();
        public static Hashtable tablas = new Hashtable();
        public static Hashtable objetos = new Hashtable();
        public static Hashtable metodos = new Hashtable();
        static ManejarArchivo master = new ManejarArchivo();

        private SistemaDeBaseDeDatos()
        {
        }

        public static SistemaDeBaseDeDatos getInstance()
        {
            return ourInstance;
        }

        public static void usarBD(String nombre)
        {
            // Llenar las bases!
            llenarBases();
            dbActual = nombre;
            if (existeBD(nombre))
            {
                // Obtener los demas listados!
                dbActual = nombre;
                llenarTablas();
                llenarMetodos();
                llenarObjetos();
               // backup_usqldump();
                //return true;
            }
            else
            {
                tablas = new Hashtable();
                objetos = new Hashtable();
                metodos = new Hashtable();
               // return false;
            }
        }

        public static void crearBD(String nombreBD, int usuario)
        {

            master.CrearCarpeta(rutaBds, "prueba");
            if (!existeBD(nombreBD))
            {
                String rutaBD = rutaBds + nombreBD + ".xml";
                String nueva = "<DB>\n" +
                               "\t<nombre>" + nombreBD + "</nombre>\n" +
                               "\t<path>\"" + rutaBD + "\"</path>\n" +
                               "\t<permisos>\n" +
                            //   "\t\t<usuario>" + Servidor.logueado.getCodigo() + "</usuario>\n" +
                               "\t</permisos>\n" +
                               "</DB>\n";
                master.modificar(rutaMaestro, nueva);
                String rutaObj = rutaBds + "obj_" + nombreBD + ".xml";
                String rutaMetodo = rutaBds + "proc_" + nombreBD + ".xml";
                master.escribir(rutaObj, "");
                master.escribir(rutaMetodo, "");
                String cuerpoBD = "<Procedure>\n" +
                                  "\t<path>\"" + rutaMetodo + "\"</path>\n" +
                                  "</Procedure>\n" +
                                  "<Object>\n" +
                                  "\t<path>\"" + rutaObj + "\"</path>\n" +
                                  "</Object>\n";
                master.escribir(rutaBD, cuerpoBD);
                Program.getGUI().appendSalida("Se creó base de datos "+ nombreBD);
                llenarBases();
            }
            else
            {
                Program.getGUI().appendSalida("ERROR, ya existe la base de datos " +nombreBD);
            }
        }

        public static bool existeBD(String nombre)
        {

                if (basesDatos.Contains(nombre))
                {
                    return true;
                }
          
            return false;
        }

        public static void crearTabla(Tabla tabla)
        {
            String rutaTabla = rutaBds + tabla.Nombre + "_" + dbActual + ".xml";
            master.escribir(rutaTabla, "");
            LinkedList<String> tablas = new LinkedList<String>();
            if (!existeTabla(tabla.Nombre))
            {
                String nueva = "<Tabla>\n" +
                               "\t<nombre>" + tabla.Nombre + "</nombre>\n" +
                               "\t<path>\"" + rutaTabla + "\"</path>\n" +
                               "\t<rows>\n";
                foreach (Campo c in tabla.Campos)
                {
                    nueva += "\t\t<campo ";
                    if (c.isNulo())
                    {
                        nueva += "nulo = \"true\" ";
                    }
                    else
                    {
                        nueva += "nulo = \"false\" ";
                    }
                    if (c.isAutoincrementable())
                    {
                        nueva += "auto = \"true\" ";
                    }
                    else
                    {
                        nueva += "auto = \"false\" ";
                    }
                    if (c.isPrimaria())
                    {
                        nueva += "primaria = \"true\" ";
                    }
                    else
                    {
                        nueva += "primaria = \"false\" ";
                    }
                    if (c.isUnica())
                    {
                        nueva += "unico = \"true\" ";
                    }
                    else
                    {
                        nueva += "unico = \"false\" ";
                    }
                    nueva += ">\n";
                    String tipo = obtenerTipo(c.Tipo);
                    nueva += "\t\t\t<" + tipo + ">" + c.Nombre + "</" + tipo + ">\n";
                    if (c.isForanea())
                    {
                        nueva += "\t\t\t<foranea>" + c.Tforanea + "</foranea>\n";
                    }
                    nueva += "\t\t</campo>\n";
                }
                nueva += "\t</rows>\n" +
                         "</Tabla>\n";
                master.modificar(rutaBds + dbActual + ".xml", nueva);
                llenarTablas();
                Program.getGUI().appendSalida("Se creó tabla " + tabla.Nombre);

                // return true;
            }
            else
            {
                // return false;
                Program.getGUI().appendSalida("ERROR, no se puede crear " + tabla.Nombre);

            }
        }


        public static bool existeTabla(String nombre)
        {
             llenarTodo();
            if (tablas.Contains(nombre))
            {
                return true;
            }

            return false;
        }

        public static String obtenerTipo(Tipos tipo)
        {
            switch (tipo)
            {
                case Tipos.INT:
                    return "int";
                case Tipos.DOUBLE:
                    return "double";
                case Tipos.STRING:
                    return "text";
                case Tipos.BOOL:
                    return "bool";
                case Tipos.DATETIME:
                    return "datetime";
                case Tipos.DATE:
                    return "date";
                default:
                    return "objeto";
            }
        }


        public static void llenarTodo()
        {
            if (!dbActual.Equals(""))
            {
                llenarTablas();
                llenarMetodos();
                llenarObjetos();
            }
        }

         public static void llenarUsuarios()
        {
           // String listado = master.leer(Servidor.rutaUsuarios);
           // usuarios = XML.getUsuarios(listado);
        }

         public static void llenarBases()
        {
            String bases = master.leer(rutaMaestro);
           // basesDatos = XML.getBasesDatos(bases);
        }

         public static void llenarTablas()
        {
            String contenido = master.leer(rutaBds + dbActual + ".xml");
           // tablas = XML.getTablas(contenido);
        }

         public static void llenarObjetos()
        {
            String contenido = master.leer(rutaBds + "obj_" + dbActual + ".xml");
           // objetos = XML.getObjetos(contenido);
        }

         public static void llenarMetodos()
        {
            String contenido = master.leer(rutaBds + "proc_" + dbActual + ".xml");
           // metodos = XML.getMetodos(contenido);
        }
    }
}
