﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static BaseDeDatos.Ast.Simbolo;

namespace BaseDeDatos.SistemaDeArchivos
{


    public class Tabla
    {

        String nombre;
        List<Campo> campos;

        public string Nombre { get => nombre; set => nombre = value; }
        public List<Campo> Campos { get => campos; set => campos = value; }

        public Tabla(string nombre)
        {
            this.Nombre = nombre;
        }

        public Tabla(string nombre, List<Campo> campos)
        {
            this.Nombre = nombre;
            this.Campos = campos;
        }



        public void agregarCampo(Campo campo)
        {
            this.Campos.Add(campo);
        }


        // verificaciones a la hora de insertar
        public int getNumeroCampos()
        {
            int num = 0;
            foreach (Campo c in Campos)
            {
                if (!c.isAutoincrementable())
                {
                    num++;
                }
            }
            return num;
        }

        public bool comprobarTipos(List<Objeto> datos)
        {
            int i = 0;
            foreach (Campo c in Campos)
            {
                if (c.isAutoincrementable())
                {
                    continue;
                }
                if (c.Tipo != datos[i].getTipo())
                {
                    return false;
                }
                i++;
            }
            return true;
        }

        // obtener el indice de la primaria si existe
        public int getIndicePrimaria()
        {
            int i = 0;
            foreach (Campo c in Campos)
            {
                if (c.isPrimaria())
                {
                    return i;
                }
                i++;
            }
            return -1;
        }

        // verificar que los campos existen y son del tipo que se pide
        public bool verificarCampos(List<String> columnas, Registro nuevo)
        {
            int i = 0;
            foreach (String col in columnas )
            {
                Tipos tipo = existeCampo(columnas[i]);
                if (tipo == Tipos.NULL || tipo != nuevo.Columnas[i].getTipo())
                {
                    return false;
                }
                i++;
            }
            return true;
        }

        // verificar que el campo existe
        public Tipos existeCampo(String nombre)
        {
            foreach (Campo c in Campos)
            {
                if (c.Nombre.Equals(nombre))
                {
                    return c.Tipo;
                }
            }
            return Tipos.NULL;
        }

        // determina si el campo es faltante
        public bool esFaltante(String nombre, List<String> columnas)
        {
            foreach (String col in columnas)
            {
                if (col.Equals(nombre))
                {
                    return false;
                }
            }
            return true;
        }

        // get indice del campo auxiliar 
        public int buscarIndice(String nombre, List<String> columnas)
        {
            int i = 0;
            foreach (String col in columnas)
            {
                if (col.Equals(nombre))
                {
                    return i;
                }
                i++;
            }
            return -1;
        }
    }
}
