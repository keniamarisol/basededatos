﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseDeDatos.SistemaDeArchivos
{
    public class Registro
    {
        private int numero;
        private List<Objeto> columnas;

        public int Numero { get => numero; set => numero = value; }
        public List<Objeto> Columnas { get => columnas; set => columnas = value; }

        public Registro(int numero, List<Objeto> columnas)
        {
            this.Numero = numero;
            this.Columnas = columnas;
        }

        public void agregarColumna(Objeto v)
        {
            Columnas.Add(v);
        }

        public Registro()
        {
            Columnas = new List<Objeto>();
        }

           

        public void modificarColumna(String nombre, Objeto objeto)
        {
            objeto.setNombre(nombre);
            int i = 0;
            foreach (Objeto col in Columnas)
            {
                if (col.getNombre().Equals(nombre))
                {
                    Columnas[i] = objeto;
                    break;
                }
                i++;
            }
        }
    }
}
