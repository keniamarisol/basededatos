﻿using Irony.Parsing;

namespace BaseDeDatos.Analizadores
{
    class GramaticaSql : Grammar
    {
        public GramaticaSql() : base(caseSensitive: false)
        {
            #region ER
            NumberLiteral numero = TerminalFactory.CreateCSharpNumber("numero");
            IdentifierTerminal id = TerminalFactory.CreateCSharpIdentifier("id");
            RegexBasedTerminal fecha = new RegexBasedTerminal("fecha", "\"[0-9]{1,2}-[0-9]{1,2}-[0-9]{4}\"");
            RegexBasedTerminal fecha_hora = new RegexBasedTerminal("fecha_hora", "\"[0-9]{1,2}-[0-9]{1,2}-[0-9]{4} [0-9]{1,2}:[0-9]{1,2}:[0-9]{1,2}\"");
            var cadena = new StringLiteral("cadena", "\"", StringOptions.AllowsLineBreak);
            var caracter = new StringLiteral("tchar", "'", StringOptions.AllowsDoubledQuote);
            //   RegexBasedTerminal variable = new RegexBasedTerminal("variable", "[@][a-ZA-z][a-zA-Z0-9_]*");
            //   RegexBasedTerminal id_arroba = new RegexBasedTerminal("id_arroba", "@[a-zA-Z]([a-zA-Z0-9]*)");
            #endregion


            #region Comentarios
            CommentTerminal comentarioMultilinea = new CommentTerminal("comentarioMultiLinea", "#*", "*#");
            base.NonGrammarTerminals.Add(comentarioMultilinea);
            CommentTerminal comentarioUnilinea = new CommentTerminal("comentarioUniLinea", "#", "\n", "\r\n");
            base.NonGrammarTerminals.Add(comentarioUnilinea);
            #endregion

            #region palabrasReservadas
            KeyTerm
            prText = ToTerm("text"),
            prInteger = ToTerm("integer"),
            prDouble = ToTerm("double"),
            prBool = ToTerm("bool"),
            prDate = ToTerm("date"),
            prDatetime = ToTerm("datetime"),


            prCrear = ToTerm("crear"),
            prBase_datos = ToTerm("base_datos"),
            prTabla = ToTerm("tabla"),
            prNulo = ToTerm("nulo"),
            prUnico = ToTerm("unico"),
            prNo_nulo = ToTerm("no nulo"),
            prAutoincremetable = ToTerm("autoincrementable"),
            prLlave_primaria = ToTerm("llave_primaria"),
            prLlave_foranea = ToTerm("llave_foranea"),
            prObjeto = ToTerm("objeto"),
            prProcedimiento = ToTerm("procedimiento"),
            prFuncion = ToTerm("funcion"),
            prRetorno = ToTerm("retorno"),
            prUsuario = ToTerm("usuario"),
            prColocar = ToTerm("colocar"),
            prPassword = ToTerm("password"),
            prUsar = ToTerm("usar"),
            prAlterar = ToTerm("alterar"),
            prAgregar = ToTerm("agregar"),
            prQuitar = ToTerm("quitar"),
            prCambiar = ToTerm("cambiar"),
            prEliminar = ToTerm("eliminar"),
            prInsertar = ToTerm("insertar"),
            prValores = ToTerm("valores"),
            prActualizar = ToTerm("actualizar"),
            prDonde = ToTerm("donde"),
            prBorrar = ToTerm("borrar"),
            prSeleccionar = ToTerm("seleccionar"),
            prDe = ToTerm(("de")),
            prOrdenar = ToTerm("ordenar_por"),
            prAsc = ToTerm("asc"),
            prDesc = ToTerm("desc"),
            prOtorgar = ToTerm("otorgar"),
            prDenegar = ToTerm("denegar"),
            prPermisos = ToTerm("permisos"),
            prDeclarar = ToTerm("declarar"),
            prSi = ToTerm("si"),
            prSiNo = ToTerm("sino"),
            prSelecciona = ToTerm("selecciona"),
            prCaso = ToTerm("caso"),
            prDefecto = ToTerm("defecto"),
            prPara = ToTerm("para"),
            prMientras = ToTerm("mientras"),
            prDetener = ToTerm("detener"),
            prImprimir = ToTerm("imprimir"),
            prFecha = ToTerm("fecha"),
            prFecha_hora = ToTerm("fecha_hora"),
            prContar = ToTerm("contar"),
            prBackupUsqldump = ToTerm("backup usqldump"),
            prUsqldump = ToTerm("usqldump"),
            prBackup = ToTerm("backup"),
            prCompleto = ToTerm("completo"),
            prRestaurar = ToTerm("restaurar"),
             prContinue = ToTerm("continue");
            // arroba = ToTerm("@");


            MarkReservedWords("text", "integer", "double", "date", "datetime", "crear", "base_datos", "tabla", "nulo", "unico", "nonulo", "autoincrementable", "llave_primaria",
                "llave_foranea", "objeto", "procedimiento", "funcion", "retorno", "usuario", "colocar", "password", "usar",
                "alterar", "agregar", "quitar", "cambiar", "eliminar", "insertar", "valores", "actualizar", "donde",
                "borrar", "seleccionar", "de", "ordenar_por", "asc", "desc", "otorgar", "denegar", "permisos", "declarar",
                "si", "sino", "selecciona", "caso", "defecto", "para", "mientras", "detener", "imprimir", "fecha", "fecha_hora",
                "contar", "backup", "usqldump", "restaurar", "completo", "permisos", "continue");

            #endregion palabrasReservadas

            #region Operadores y simbolos
            Terminal ptcoma = ToTerm(";"),
                coma = ToTerm(","),
                punto = ToTerm("."),
                dospuntos = ToTerm(":"),
                parizq = ToTerm("("),
                parder = ToTerm(")"),
                llaizq = ToTerm("{"),
                llader = ToTerm("}"),
                signo_mas = ToTerm("+"),
                signo_menos = ToTerm("-"),
                signo_por = ToTerm("*"),
                signo_div = ToTerm("/"),
                signo_pot = ToTerm("^"),
                igual_que = ToTerm("=="),
                diferente_que = ToTerm("!="),
                menor_que = ToTerm("<"),
                mayor_que = ToTerm(">"),
                menorigual_que = ToTerm("<="),
                mayorigual_que = ToTerm(">="),
                pr_or = ToTerm("||"),
                pr_and = ToTerm("&&"),
                pr_not = ToTerm("!"),
                pr_true = ToTerm("true", "true"),
                pr_false = ToTerm("false", "false"),
                igual = ToTerm("="),
                interrogacion = ToTerm("?"),
                corizq = ToTerm("["),
                corder = ToTerm("]");
            #endregion

            #region No terminales
            NonTerminal S = new NonTerminal("S"),
             TIPO = new NonTerminal("TIPO"),
             EXPRESION = new NonTerminal("EXPRESION"),
             OPMAT = new NonTerminal("OPMAT"),
             SENTENCIA = new NonTerminal("SENTENCIA"),
             SENTENCIA_LISTA = new NonTerminal("SENTENCIA_LISTA"),
             ASIGNACION = new NonTerminal("ASIGNACION"),
             VISIBILIDAD = new NonTerminal("VISIBILIDAD");
            #endregion

            var PROGRAMA = new NonTerminal("PROGRAMA");
            var INSTRUCCION = new NonTerminal("INSTRUCCION");
            var INSTRUCCION_LISTA = new NonTerminal("INSTRUCCION_LISTA");
            var CREAR_BASE = new NonTerminal("CREAR_BASE");
            var CREAR_TABLA = new NonTerminal("CREAR_TABLA");
            var CREAR_OBJETO = new NonTerminal("CREAR_OBJETO");
            var CREAR_PROC = new NonTerminal("CREAR_PROC");
            var CREAR_FUN = new NonTerminal("CREAR_FUN");
            var RETORNO = new NonTerminal("RETORNO");
            var VARIABLE = new NonTerminal("VARIABLE");
            var VARIABLE_LISTA = new NonTerminal("VARIABLE_LIST");
            var CALL_FUN = new NonTerminal("CALL_FUN");
            var CREAR_USU = new NonTerminal("CREAR_USU");
            var USAR = new NonTerminal("USAR");
            var ALTERAR = new NonTerminal("ALTERAR");
            var ALTERAR_TAB = new NonTerminal("ALTERAR_TAB");
            var ALTERAR_OBJ = new NonTerminal("ALTERAR_OBJ");
            var ALTERAR_USU = new NonTerminal("ALTERAR_USU");
            var ELIMINAR = new NonTerminal("ELIMINAR");
            var INSERTAR = new NonTerminal("INSERTAR");
            var INSERTAR_ESPECIAL = new NonTerminal("INSERTAR_ESPECIAL");
            var ACTUALIZAR = new NonTerminal("BORRAR_ENTAB");
            var BORRAR_ENTAB = new NonTerminal("ACTUALIZAR");
            var SELECCIONAR = new NonTerminal("SELECCIONAR");
            var OTORGAR = new NonTerminal("OTORGAR");
            var DENEGAR = new NonTerminal("DENEGAR");
            var DECLARAR = new NonTerminal("DECLARAR");
            var CAMPO_LISTA = new NonTerminal("CAMPO_LISTA");
            var CAMPO = new NonTerminal("CAMPO");
            var COMPLEMENTO = new NonTerminal("COMPLEMENTO");
            var COMPLEMENTO_LISTA = new NonTerminal("COMPLEMENTO_LISTA");
            var ATRIBUTO = new NonTerminal("ATRIBUTO");
            var ATRIBUTO_LISTA = new NonTerminal("ATRIBUTO_LISTA");
            var PARAMETRO = new NonTerminal("PARAMETRO");
            var PARAMETRO_LISTA = new NonTerminal("PARAMETRO_LISTA");
            var EXPRESION_LISTA = new NonTerminal("EXPRESION_LISTA");
            var ID_LISTA = new NonTerminal("ID_LISTA");
            var CREAR = new NonTerminal("CREAR");
            var DECLARAR_OBJETO = new NonTerminal("DECLARAR_OBJETO");
            var ACCESO_OBJETO = new NonTerminal("ACCESO_OBJETO");
            var ASIGNACION_OBJETO = new NonTerminal("ASIGNACION_OBJETO");
            var SI = new NonTerminal("SI");
            var SISINO_SIMP = new NonTerminal("SISINO_SIMP");
            var SINOSI = new NonTerminal("SINOSI");
            var SINO = new NonTerminal("SINO");
            var SINOSI_LIST = new NonTerminal("SINOSI_LIST");
            var MIENTRAS = new NonTerminal("MIENTRAS");
            var PARA = new NonTerminal("PARA");
            var SELECCIONA = new NonTerminal("PARA");
            var CASO = new NonTerminal("CASO");
            var CASO_LISTA = new NonTerminal("CASO_LISTA");
            var DECLARAR_ASIG = new NonTerminal("DECLARAR_ASIG");
            var PARA_COND = new NonTerminal("PARA_COND");
            var DECREMENTOS = new NonTerminal("DECREMENTOS");
            var IMPRIMIR = new NonTerminal("IMPRIMIR");
            var BACKUP = new NonTerminal("BACKUP");
            var RESTAURAR = new NonTerminal("RESTAURAR");
            var PRIMITIVO = new NonTerminal("PRIMITIVO");
            var EXPRESION_ARITMETICA = new NonTerminal("EXPRESION_ARITMETICA");
            var EXPRESION_RELACIONAL = new NonTerminal("EXPRESION_RELACIONAL");
            var EXPRESION_LOGICA = new NonTerminal("EXPRESION_LOGICA");
            var BREAK = new NonTerminal("BREAK");
            var CONTINUE = new NonTerminal("CONTINUE");
            var INCREMENTO = new NonTerminal("INCREMENTO");
            var DDL = new NonTerminal("DDL");
            var DML = new NonTerminal("DML");
            var SSL = new NonTerminal("SSL");
            var DCL = new NonTerminal("DCL");
            var FUNCION_FECHA = new NonTerminal("FUNCION_FECHA");
            var FUNCION_FECHAHORA = new NonTerminal("FUNCION_FECHAHORA");
            var CONTAR = new NonTerminal("CONTAR");
            var OBJETO_LISTA = new NonTerminal("OBJETO_LISTA");
            var BASE_DATOS = new NonTerminal("BASE_DATOS");
            var LLAVE_FORANEA = new NonTerminal("LLAVE_FORANEA");

            #region inicio
            PROGRAMA.Rule = INSTRUCCION_LISTA;
            #endregion fin inicio

            INSTRUCCION_LISTA.Rule = MakePlusRule(INSTRUCCION_LISTA, INSTRUCCION);

            INSTRUCCION.Rule =
                 DDL
                | DML
                | DCL
                | SSL;

            SENTENCIA_LISTA.Rule = MakePlusRule(SENTENCIA_LISTA, SENTENCIA);

            SENTENCIA.Rule =
                DDL
                | DML
                | DCL
                | SSL
                ;

 

            DDL.Rule = USAR + ptcoma
                | CREAR_BASE + ptcoma
                | CREAR_OBJETO + ptcoma
                | CREAR_TABLA + ptcoma
                | CREAR_USU + ptcoma
                | CREAR_FUN
                | CREAR_PROC
                | CALL_FUN + ptcoma
                | ALTERAR + ptcoma
                | ELIMINAR + ptcoma;

            DML.Rule = INSERTAR + ptcoma
                | INSERTAR_ESPECIAL
                | ACTUALIZAR + ptcoma
                | BORRAR_ENTAB + ptcoma
                | SELECCIONAR + ptcoma;

            DCL.Rule = OTORGAR + ptcoma
                | DENEGAR + ptcoma
               ;

            SSL.Rule = DECLARAR + ptcoma
                | DECLARAR_ASIG + ptcoma
                | ASIGNACION + ptcoma
                | RETORNO + ptcoma
                | SELECCIONAR + ptcoma
                | SI
                | PARA
                | SELECCIONA
                | MIENTRAS
                | IMPRIMIR + ptcoma
                | BACKUP + ptcoma
                | RESTAURAR + ptcoma
                | BREAK + ptcoma
                | CONTINUE + ptcoma
                | CONTAR + ptcoma;

            CREAR_BASE.Rule = prCrear + prBase_datos + id;

            CREAR_TABLA.Rule = prCrear + prTabla + id + parizq + CAMPO_LISTA + parder;

            CREAR_OBJETO.Rule = prCrear + prObjeto + id + parizq + ATRIBUTO_LISTA + parder;

            CREAR_USU.Rule = prCrear + prUsuario + id + prColocar + prPassword + igual + cadena;

         /*   CREAR.Rule = prCrear + prBase_datos + id
                | prCrear + prTabla + id + parizq + CAMPO_LISTA + parder
                | prCrear + prObjeto + id + parizq + ATRIBUTO_LISTA + parder
                | prCrear + prUsuario + id + prColocar + prPassword + igual + cadena
                ;*/

            CREAR_FUN.Rule = prCrear + prFuncion + id + parizq + PARAMETRO_LISTA + parder + TIPO + llaizq + SENTENCIA_LISTA + llader;

            CREAR_PROC.Rule = prCrear + prProcedimiento + id + parizq + PARAMETRO_LISTA + parder + llaizq + SENTENCIA_LISTA + llader;

            CAMPO_LISTA.Rule = MakePlusRule(CAMPO_LISTA, ToTerm(","), CAMPO);

            CAMPO.Rule = TIPO + id + COMPLEMENTO_LISTA;

            COMPLEMENTO_LISTA.Rule = MakeStarRule(COMPLEMENTO_LISTA, COMPLEMENTO);

            COMPLEMENTO.Rule = prNulo | prNo_nulo | prAutoincremetable | prLlave_primaria
                          | LLAVE_FORANEA | prUnico;


            LLAVE_FORANEA.Rule = prLlave_foranea + id + id;

            ATRIBUTO_LISTA.Rule = MakePlusRule(ATRIBUTO_LISTA, ToTerm(","), ATRIBUTO);

            ATRIBUTO.Rule = TIPO + id;

            PARAMETRO_LISTA.Rule = MakePlusRule(PARAMETRO_LISTA, ToTerm(","), PARAMETRO);

            PARAMETRO.Rule = TIPO + id;

            RETORNO.Rule = prRetorno + EXPRESION;

            BREAK.Rule = prDetener;

            CONTINUE.Rule = prContinue;

            EXPRESION_LISTA.Rule = MakePlusRule(EXPRESION_LISTA, ToTerm(","), EXPRESION);

            USAR.Rule = prUsar + id;

            ID_LISTA.Rule = MakePlusRule(ID_LISTA, ToTerm(","), id);


            ALTERAR.Rule = prAlterar + prTabla + id + prAgregar + parizq + CAMPO_LISTA + parder
                | prAlterar + prTabla + id + prQuitar + ID_LISTA
                | prAlterar + prObjeto + id + prAgregar + parizq + CAMPO_LISTA + parder
                | prAlterar + prObjeto + id + prQuitar + ID_LISTA
                | prAlterar + prUsuario + id + prCambiar + prPassword + igual + cadena;


            ELIMINAR.Rule = prEliminar + prTabla + id
                | prEliminar + prBase_datos + id
                | prEliminar + prObjeto + id
                | prEliminar + prUsuario + id;


            INSERTAR.Rule = prInsertar + ToTerm("en") + prTabla + id + parizq + EXPRESION_LISTA + parder
                          ;


            INSERTAR_ESPECIAL.Rule = prInsertar + ToTerm("en") + prTabla + id + parizq + EXPRESION_LISTA + parder + prValores + parizq + EXPRESION_LISTA + parder; 

            ACTUALIZAR.Rule = prActualizar + prTabla + id + parizq + ID_LISTA + parder +
                prValores + parizq + EXPRESION_LISTA + parder + prDonde + EXPRESION
                | prActualizar + prTabla + id + parizq + ID_LISTA + parder +
                prValores + parizq + EXPRESION_LISTA + parder;

            BORRAR_ENTAB.Rule = prBorrar + ToTerm("en") + prTabla + id + prDonde + EXPRESION
                | prBorrar + ToTerm("en") + prTabla + id;

            SELECCIONAR.Rule = prSeleccionar + ID_LISTA + prDe + ID_LISTA
            | prSeleccionar + ID_LISTA + prDe + ID_LISTA + prOrdenar + id + (prDesc | prAsc)
            | prSeleccionar + ID_LISTA + prDe + ID_LISTA + prDonde + EXPRESION
            | prSeleccionar + ID_LISTA + prDe + ID_LISTA + prDonde + EXPRESION + prOrdenar + id + (prDesc | prAsc)
            | prSeleccionar + signo_por + prDe + ID_LISTA
            | prSeleccionar + signo_por + prDe + ID_LISTA + prOrdenar + id + (prDesc | prAsc)
            | prSeleccionar + signo_por + prDe + ID_LISTA + prDonde + EXPRESION
            | prSeleccionar + signo_por + prDe + ID_LISTA + prDonde + EXPRESION + prOrdenar + id + (prDesc | prAsc)
            ;



            DECLARAR.Rule = prDeclarar + ID_LISTA + TIPO;

            DECLARAR_ASIG.Rule = prDeclarar + ID_LISTA + TIPO + igual + EXPRESION;

            ASIGNACION.Rule = ID_LISTA + igual + EXPRESION
                | ACCESO_OBJETO + igual + EXPRESION;

            // VARIABLE_LISTA.Rule = MakePlusRule(VARIABLE_LISTA, ToTerm(","), id);

            // VARIABLE.Rule =  id;

            OTORGAR.Rule = prOtorgar + prPermisos + id + coma + id + punto + (signo_por | id);

            DENEGAR.Rule = prDenegar + prPermisos + id + coma + id + punto + (signo_por | id);

            DECLARAR_OBJETO.Rule = prDeclarar + id + id;

            ACCESO_OBJETO.Rule = id + punto + id;

            CALL_FUN.Rule = id + parizq + EXPRESION_LISTA + parder;

            SI.Rule = prSi + parizq + EXPRESION + parder + llaizq + SENTENCIA_LISTA + llader +
                prSiNo + llaizq + SENTENCIA_LISTA + llader
                 | prSi + parizq + EXPRESION + parder + llaizq + SENTENCIA_LISTA + llader;

            MIENTRAS.Rule = prMientras + parizq + EXPRESION + parder + llaizq + SENTENCIA_LISTA + llader;

            PARA.Rule = prPara + parizq + DECLARAR_ASIG + ptcoma + EXPRESION + ptcoma + INCREMENTO
                + parder + llaizq + SENTENCIA_LISTA + llader;

            INCREMENTO.Rule = ToTerm("--") | ToTerm("++");

            SELECCIONA.Rule = prSelecciona + parizq + EXPRESION + parder + llaizq
                + CASO_LISTA +
                 prDefecto + dospuntos + SENTENCIA_LISTA + llader
                | prSelecciona + parizq + EXPRESION + parder + llaizq
                + CASO_LISTA + llader;

            CASO.Rule = prCaso + PRIMITIVO + dospuntos + SENTENCIA_LISTA;

            CASO_LISTA.Rule = MakePlusRule(CASO_LISTA, CASO);

            IMPRIMIR.Rule = prImprimir + parizq + EXPRESION + parder;

            FUNCION_FECHA.Rule = prFecha + parizq + parder;

            FUNCION_FECHAHORA.Rule = prFecha_hora + parizq + parder;

            CONTAR.Rule = prContar + parizq + ToTerm("<<") + SELECCIONAR + ToTerm(">>") + parder;

            BACKUP.Rule = prBackup + prUsqldump + id + id
                | prBackup + prCompleto + id + id;

            RESTAURAR.Rule = prRestaurar + prUsqldump + cadena
               | prRestaurar + prCompleto + cadena;

            TIPO.Rule =
                 prInteger
               | prDouble
               | prText
               | prBool
               | prDate
               | prDatetime
               | id;

            PRIMITIVO.Rule = id | numero | cadena;

            EXPRESION.Rule = EXPRESION_ARITMETICA
               | EXPRESION_LOGICA
               | EXPRESION_RELACIONAL
               | PRIMITIVO
               | parizq + EXPRESION + parder
               | FUNCION_FECHA
               | FUNCION_FECHAHORA
               | fecha
               | fecha_hora
               | ACCESO_OBJETO
               | CALL_FUN
               | CONTAR;


            EXPRESION_ARITMETICA.Rule = signo_menos + EXPRESION
                | EXPRESION + signo_mas + EXPRESION
                | EXPRESION + signo_menos + EXPRESION
                | EXPRESION + signo_por + EXPRESION
                | EXPRESION + signo_div + EXPRESION
                | EXPRESION + signo_pot + EXPRESION;

            EXPRESION_RELACIONAL.Rule = EXPRESION + mayor_que + EXPRESION
                | EXPRESION + menor_que + EXPRESION
                | EXPRESION + menorigual_que + EXPRESION
                | EXPRESION + mayorigual_que + EXPRESION
                | EXPRESION + igual_que + EXPRESION
                | EXPRESION + diferente_que + EXPRESION;

            EXPRESION_LOGICA.Rule = EXPRESION + pr_or + EXPRESION
                | EXPRESION + pr_and + EXPRESION
                | pr_not + EXPRESION;


            /* EXPRESION.Rule = EXPRESION + or + EXPRESION
                 | EXPRESION + and + EXPRESION
                 | EXPRESION + igualQue + EXPRESION
                 | EXPRESION + diferenteQue + EXPRESION
                 | EXPRESION + mayorQue + EXPRESION
                 | EXPRESION + menorQue + EXPRESION
                 | EXPRESION + mayorIgualQue + EXPRESION
                 | EXPRESION + menorIgualQue + EXPRESION
                 | EXPRESION + signoMas + EXPRESION
                 | EXPRESION + signoMenos + EXPRESION
                 | signoMas + EXPRESION
                 | signoMenos + EXPRESION
                 | EXPRESION + signoMas + signoMas
                 | EXPRESION + signoMenos + signoMenos
                 | EXPRESION + signoPor + EXPRESION
                 | EXPRESION + signoDiv + EXPRESION
                 | EXPRESION + signoPot + EXPRESION
                 | EXPRESION + signoMod + EXPRESION
                 | parder + EXPRESION + parizq
                 | not + EXPRESION
                 | VARIABLE
                 | ACCESO_OBJETO
                 | CALL_FUN
                 | PRIMITIVO;*/

            #region Preferencias
            this.Root = PROGRAMA;
            this.MarkTransient(INSTRUCCION,  SENTENCIA, DDL, DCL, SSL, DML, COMPLEMENTO);

            //---------------------> Eliminacion de caracters, no terminales
            this.MarkPunctuation("(", ")", ";", ":", "{", "}", "=", ".", ",", "[", "]");
            this.MarkPunctuation("repetir", "until", "imprimir", "principal", "retorno", "este");
            this.MarkPunctuation("Si", "Sino", "Sino Si", "Mientras", "Hacer", "para", 
                "de", "en", "declarar", "procedimiento", "funcion", "crear", "eliminar", "alterar",
                "base_datos", "usar", "tabla", "objeto", "usuario", "complemento",
                "insertar", "actualizar");
            #endregion


            #region precedencia
            /*
             * En esta region se define la precedencia y asociatividad para remover la ambiguedad
             * de la GramaticaSql de EXPRESIONes.             
            */
            RegisterOperators(1, Associativity.Right, igual);
            RegisterOperators(2, Associativity.Left, pr_or);
            RegisterOperators(4, Associativity.Left, pr_and);
            RegisterOperators(5, Associativity.Left, igual_que, diferente_que);
            RegisterOperators(6, Associativity.Left, mayor_que, menor_que, mayorigual_que, menorigual_que);
            RegisterOperators(7, Associativity.Left, signo_mas, signo_menos);
            RegisterOperators(8, Associativity.Left, signo_por, signo_div);
            RegisterOperators(9, Associativity.Right, signo_pot);
            RegisterOperators(10, Associativity.Right, pr_not);
            RegisterOperators(11, Associativity.Left, punto);
            RegisterOperators(12, Associativity.Neutral, parizq, parder);
            #endregion

        }
    }
}
