﻿using Irony.Parsing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseDeDatos.Analizadores
{
    public class InterpreteP
    {

        public void ejecutar(ParseTreeNode nodo)
        {
            switch (nodo.Term.Name)
            {
                case "PAQUETE_LISTA":
                    foreach (var item in nodo.ChildNodes)
                    {
                        ejecutar(item);
                    }
                    break;
                case "PROGRAMA":
                    ejecutar(nodo.ChildNodes[0]);
                    break;
                case "PAQUETE_LOGIN":
                    //ejecutar(nodo.ChildNodes[2]);
                    break;
                case "PAQUETE_SQL":
                    String text = nodo.ChildNodes[0].Token.Text;
                    text = text.Replace("~","");
                    CompilarSql.compilar(text);
                    break;
                case "PAQUETE_FIN":
                    //ejecutar(nodo.ChildNodes[2]);
                    break;
                case "PAQUETE_REPORTE":
                    //  ejecutar(nodo);
                    break;

                default: break;
            }
        }
    }
}
