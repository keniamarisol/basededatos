﻿using BaseDeDatos.Ast;
using BaseDeDatos.Ast.SSL;
using BaseDeDatos.Graficar;
using Irony.Parsing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BaseDeDatos.Analizadores
{
    public class CompilarSql
    {
        public static void compilar(string entrada)
        {
            Program.getGUI().appendSalida("Cadena de entrada sintacticamente correcta \n" + entrada);

            GramaticaSql gram = new GramaticaSql();
            LanguageData lenguaje = new LanguageData(gram);
            Parser parser = new Parser(lenguaje);

            try
            {
                ParseTree arbol = parser.Parse(entrada);
                ParseTreeNode raiz = arbol.Root;

                if (raiz == null || arbol.ParserMessages.Count > 0 || arbol.HasErrors())
                {
                    foreach (var item in arbol.ParserMessages)
                    {
                        Program.getGUI().appendSalida("Linea:" + (Convert.ToInt32(item.Location.Line) + 1).ToString() + ", Colum:" + item.Location.Column + ", Msg:" + item.Message + "\r\n");
                    }
                }
                else
                {
                    generarImagen(raiz);
                    Program.getGUI().appendSalida("Cadena de entrada sintacticamente correcta \n");
                    InterpreteSql an = new InterpreteSql();
                    AST auxArbol = an.Analizar(arbol.Root);
                   
                    Entorno global = new Entorno(null);
                    try
                    {
                        if (auxArbol != null)
                        {
                            foreach (Instruccion ins in auxArbol.Instrucciones)
                            {
                                if (ins is Funcion)
                                {
                                    Funcion funcion = (Funcion)ins;
                                    global.agregar(funcion.Identificador, funcion);
                 
                                }
                                else if (ins is Declaracion)
                                {
                                    Declaracion declaracion = (Declaracion)ins;
                                    declaracion.ejecutar(global, auxArbol);
                                }
                                else {
                                    ((Instruccion)ins).ejecutar(global, auxArbol);
                                }
                            }
                         
                        }
                        else
                        {
                            MessageBox.Show("El arbol de Irony no se construyo. Cadena invalida!\n");
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());

                    }
                }

            }
            catch (Exception eX)
            {
                MessageBox.Show(eX.ToString());
            }

        }

        private static void generarImagen(ParseTreeNode raiz)
        {
            String graf = graficar.getdot(raiz);
            WINGRAPHVIZLib.DOT dot = new WINGRAPHVIZLib.DOT();
            WINGRAPHVIZLib.BinaryImage img = dot.ToPNG(graf);
            img.Save("AST.png");
        }
    }
}
