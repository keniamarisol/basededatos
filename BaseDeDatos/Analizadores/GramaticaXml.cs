﻿using Irony.Parsing;

namespace BaseDeDatos.Analizadores
{
    class GramaticaXml : Grammar
    {
        public GramaticaXml() : base(caseSensitive: false)
        {
            #region ER
            NumberLiteral numero = TerminalFactory.CreateCSharpNumber("numero");
            IdentifierTerminal id = TerminalFactory.CreateCSharpIdentifier("id");
           
            var cadena = new StringLiteral("cadena", "\"", StringOptions.AllowsLineBreak);
            var caracter = new StringLiteral("tchar", "'", StringOptions.AllowsDoubledQuote);
            //   RegexBasedTerminal variable = new RegexBasedTerminal("variable", "[@][a-ZA-z][a-zA-Z0-9_]*");
            //   RegexBasedTerminal id_arroba = new RegexBasedTerminal("id_arroba", "@[a-zA-Z]([a-zA-Z0-9]*)");
            #endregion

            #region palabrasReservadas
            KeyTerm
            prDbA = ToTerm("<DB>"),
            prDbC = ToTerm("</DB>"),
            prNombreA = ToTerm("<nombre>"),
            prNombreC = ToTerm("<nombre>"),
            prPathA = ToTerm("<path>"),
            prPathC = ToTerm("</path>"),
            prProcA = ToTerm("<Procedure>"),
            prProcC = ToTerm("</Procedure>"),
            prObjA = ToTerm("<Object>"),
            prObjC = ToTerm("</Object>"),
            prTablaA = ToTerm("<Tabla>"),
            prTablaC = ToTerm("</Tabla>"),
            prRowA = ToTerm("<rows>"),
            prRowC = ToTerm("</rows>"),
            prParamA = ToTerm("<params>"),
            prParamC = ToTerm("</params>"),
            prIntA = ToTerm("<int>"),
            prIntC = ToTerm("</int>"),
            prTextA = ToTerm("<text>"),
            prTextC = ToTerm("</text>"),
            prBoolA = ToTerm("<bool>"),
            prBoolC = ToTerm("</bool>"),
            prSrcA = ToTerm("</src>"),
            prSrcC = ToTerm("</src>"),
            prAtrrA = ToTerm("<attr>"),
            prAtrrC = ToTerm("</attr>");



            MarkReservedWords("<DB>", "</DB>", "<nombre>", "<nombre>", "<path>", "</path>", "<Procedure>", 
                            "</Procedure>", "<Object>", "</Object>",
                            "<Tabla>", "</Tabla>", "<path>", "</path>", "<rows>", "</rows>", "<params>", 
                            "</params>", "<int>", "</int>", "<text>",
                            "</text>", "<bool>", "</bool>", "</src>", "<attr>", "</attr>");

            #endregion palabrasReservadas

            #region Operadores y simbolos
            Terminal ptcoma = ToTerm(";"),
                coma = ToTerm(","),
                punto = ToTerm("."),
                dospuntos = ToTerm(":"),
                parizq = ToTerm("("),
                parder = ToTerm(")"),
                llaizq = ToTerm("{"),
                llader = ToTerm("}"),
                signo_mas = ToTerm("+"),
                signo_menos = ToTerm("-"),
                signo_por = ToTerm("*"),
                signo_div = ToTerm("/"),
                signo_pot = ToTerm("^"),
                igual_que = ToTerm("=="),
                diferente_que = ToTerm("!="),
                menor_que = ToTerm("<"),
                mayor_que = ToTerm(">"),
                pr_or = ToTerm("||"),
                pr_and = ToTerm("&&"),
                pr_not = ToTerm("!"),
                pr_true = ToTerm("true", "true"),
                pr_false = ToTerm("false", "false"),
                igual = ToTerm("="),
                interrogacion = ToTerm("?"),
                corizq = ToTerm("["),
                corder = ToTerm("]");
            #endregion

            #region No terminales
            NonTerminal S = new NonTerminal("S"),
             TIPO = new NonTerminal("TIPO"),
             EXPRESION = new NonTerminal("EXPRESION"),
             OPMAT = new NonTerminal("OPMAT"),
             SENTENCIA = new NonTerminal("SENTENCIA"),
             SENTENCIA_LISTA = new NonTerminal("SENTENCIA_LISTA"),
             ASIGNACION = new NonTerminal("ASIGNACION"),
             VISIBILIDAD = new NonTerminal("VISIBILIDAD");
            #endregion

            var PROGRAMA = new NonTerminal("PROGRAMA");
            var PAQUETE = new NonTerminal("PAQUETE");


            #region inicio
            /*   PROGRAMA.Rule = PAQUETE_LISTA;
               #endregion fin inicio

               PAQUETE_LISTA.Rule = MakePlusRule(PAQUETE_LISTA, PAQUETE);

               PAQUETE.Rule = PAQUETE_LOGIN | PAQUETE_FIN | PAQUETE_REPORTE | PAQUETE_SQL;

           */
            #endregion

            #region Preferencias
            this.Root = PROGRAMA;
            this.MarkTransient(PAQUETE);

            //---------------------> Eliminacion de caracters, no terminales
            this.MarkPunctuation("(", ")", ";", ":", "{", "}", "=", ".", ",", "[", "]");
            this.MarkPunctuation("repetir", "\"validar\"", "\"login\"", "\"password\"", "\"paquete\"", "\"fin\"", "\"usql\"", "\"instruccion\"", "\"reporte\"");

            #endregion


        }
    }
}

