﻿using BaseDeDatos.Ast;
using BaseDeDatos.Ast.DDL;
using BaseDeDatos.Ast.SSL;
using BaseDeDatos.SistemaDeArchivos;
using Irony.Parsing;
using System;
using System.Collections.Generic;
using static BaseDeDatos.Ast.Simbolo;

namespace BaseDeDatos.Analizadores
{

    public class InterpreteSql
    {
        

        public AST Analizar(ParseTreeNode raiz)
        {
          //  LinkedList<NodoAST> v = (LinkedList<NodoAST>)auxiliar(raiz);
             return (AST)auxiliar(raiz);
          //  return new AST(v);
        }


        public object auxiliar(ParseTreeNode actual)
        {
            if (EstoyAca(actual, "PROGRAMA"))
            {
                return auxiliar(actual.ChildNodes[0]);
            }
            else if (EstoyAca(actual, "INSTRUCCION_LISTA"))
            {
                LinkedList<Instruccion> instrucciones = new LinkedList<Instruccion>();
                foreach (ParseTreeNode hijo in actual.ChildNodes)
                {
                    instrucciones.AddLast((Instruccion)auxiliar(hijo));
                }
                return new AST(instrucciones);
            }
            else if (EstoyAca(actual, "CREAR_BASE"))
            {
                return new CrearBase(getLexema(actual, 0));
            }
            else if (EstoyAca(actual, "USAR"))
            {
                return new Usar(getLexema(actual, 0));
            }
            else if (EstoyAca(actual, "CREAR_TABLA"))
            {
                return new CrearTabla(getLexema(actual, 0), (List<Campo>)auxiliar(actual.ChildNodes[1]));
            }
            else if (EstoyAca(actual, "CAMPO_LISTA"))
            {
                List<Campo> campos = new List<Campo>();
                foreach (ParseTreeNode hijo in actual.ChildNodes)
                {
                    campos.Add((Campo)auxiliar(hijo));

                }
                return campos;
            }
            else if (EstoyAca(actual, "CAMPO"))
            {
                Campo camp = new Campo(getLexema(actual, 1), (Tipos)auxiliar(actual.ChildNodes[0]));
                foreach (ParseTreeNode nodo in actual.ChildNodes[2].ChildNodes)
                {
                    switch (nodo.Term.Name.ToLower())
                    {
                        case "llave_primaria":
                            camp.setPrimaria(true);
                            break;
                        case "autoincrementable":
                            camp.setAutoincrementable(true);
                            break;
                        case "no nulo":
                            camp.setNulo(false);
                            break;
                        case "nulo":
                            camp.setNulo(true);
                            break;
                        case "llave_foranea":
                            camp.setForanea(nodo.ChildNodes[1].Token.Text);
                            break;
                        default:
                            camp.setUnica(true);
                            break;
                    }

                }

                return camp;

            }
            else if (EstoyAca(actual, "PARAMETRO_LISTA"))
            {
                LinkedList<Simbolo> parametros = new LinkedList<Simbolo>();
                foreach (ParseTreeNode hijo in actual.ChildNodes)
                {
                    parametros.AddLast((Simbolo)auxiliar(hijo));
                }
                return parametros;
            }
            else if (EstoyAca(actual, "PARAMETRO"))
            {
                return new Simbolo((Tipos)auxiliar(actual.ChildNodes[0]), getLexema(actual, 1));
            }
            else if (EstoyAca(actual, "CREAR_FUN"))
            {
                return new Funcion((Tipos)auxiliar(actual.ChildNodes[2]), getLexema(actual, 0), (LinkedList<Simbolo>)auxiliar(actual.ChildNodes[1]), (LinkedList<NodoAST>)auxiliar(actual.ChildNodes[3]));
            }
            else if (EstoyAca(actual, "CREAR_PROC"))
            {
                return new Funcion(Tipos.NULL, getLexema(actual, 0), (LinkedList<Simbolo>)auxiliar(actual.ChildNodes[1]), (LinkedList<NodoAST>)auxiliar(actual.ChildNodes[2]));
            }
            else if (EstoyAca(actual, "CALL_FUN"))
            {
                //aqui se debe de crear un entrno antes de ejcutar la llamada
                if (actual.ChildNodes.Count == 2)
                {
                    return new Llamada(getLexema(actual, 0), (LinkedList<Expresion>)auxiliar(actual.ChildNodes[1]));
                }
                else
                {
                    return new Llamada(getLexema(actual, 0));
                }
            }
            else if (EstoyAca(actual, "EXPRESION_LISTA"))
            {
                LinkedList<Expresion> argumentos = new LinkedList<Expresion>();
                foreach (ParseTreeNode hijo in actual.ChildNodes)
                {
                    argumentos.AddLast((Expresion)auxiliar(hijo));
                }
                return argumentos;
            }
            else if (EstoyAca(actual, "SENTENCIA_LISTA"))
            {
                LinkedList<NodoAST> sentencias = new LinkedList<NodoAST>();
                foreach (ParseTreeNode hijo in actual.ChildNodes)
                {
                    sentencias.AddLast((NodoAST)auxiliar(hijo));

                }
                //return sentencias;
                return sentencias;
                //return auxiliar(sentencias);
            }
            else if (EstoyAca(actual, "RETORNO"))
            {
                if (actual.ChildNodes.Count == 1)
                {
                    return new Return((Expresion)auxiliar(actual.ChildNodes[0]));
                }
                else
                {
                    return new Return();
                }
            }
            else if (EstoyAca(actual, "BREAK"))
            {
                return new Break();
            }
            else if (EstoyAca(actual, "CONTINUE"))
            {
                return new Continue();

            }
            else if (EstoyAca(actual, "DECLARAR"))
            {
                return new Declaracion((Tipos)auxiliar(actual.ChildNodes[1]), (LinkedList<Simbolo>)auxiliar(actual.ChildNodes[0]), null);
            }
            else if (EstoyAca(actual, "DECLARAR_ASIG"))
            {
                return new Declaracion((Tipos)auxiliar(actual.ChildNodes[1]), (LinkedList<Simbolo>)auxiliar(actual.ChildNodes[0]), (Expresion)auxiliar(actual.ChildNodes[2]));
            }
            else if (EstoyAca(actual, "ASIGNACION"))
            {
                if (actual.ChildNodes.Count == 2)
                {
                    return new Asignacion((LinkedList<Simbolo>)auxiliar(actual.ChildNodes[0]), (Expresion)auxiliar(actual.ChildNodes[1]));
                }
                else
                {
                    return new Asignacion(getLexema(actual, 0), getLexema(actual, 1), (Expresion)auxiliar(actual.ChildNodes[2]));
                }

            }
            else if (EstoyAca(actual, "ID_LISTA"))
            {
                LinkedList<Simbolo> simbolos = new LinkedList<Simbolo>();
                foreach (ParseTreeNode hijo in actual.ChildNodes)
                {
                    simbolos.AddLast(new Simbolo(hijo.Token.Text.Replace("@", "")));
                }
                return simbolos;
            }
            else if (EstoyAca(actual, "SI"))
            {
                if (actual.ChildNodes.Count == 3)
                {
                    return new If((Expresion)auxiliar(actual.ChildNodes[0]), (LinkedList<NodoAST>)auxiliar(actual.ChildNodes[1]), (LinkedList<NodoAST>)auxiliar(actual.ChildNodes[2]));
                }
                else
                {
                    return new If((Expresion)auxiliar(actual.ChildNodes[0]), (LinkedList<NodoAST>)auxiliar(actual.ChildNodes[1]), new LinkedList<NodoAST>());
                }
            }
            else if (EstoyAca(actual, "MIENTRAS"))
            {
                
                return new While((LinkedList<NodoAST>)auxiliar(actual.ChildNodes[1]), (Expresion)auxiliar(actual.ChildNodes[0]));
            }
            else if (EstoyAca(actual, "PARA"))
            {
                return new For((LinkedList<NodoAST>)auxiliar(actual.ChildNodes[3]), (Declaracion)auxiliar(actual.ChildNodes[0]), actual.ChildNodes[0].ChildNodes[0].ChildNodes[0].Token.Text.Replace("@", ""), (Expresion)auxiliar(actual.ChildNodes[1]),(For.Incremento)auxiliar(actual.ChildNodes[2]));
            }
            else if (EstoyAca(actual, "INCREMENTO"))
            {

                if (actual.ChildNodes[0].Token.Text.Equals("--"))
                {
                    return For.Incremento.DECREMENTO;
                }
                return For.Incremento.INCREMENTO;
            }
            else if (EstoyAca(actual, "IMPRIMIR"))
            {
                return new Imprimir((Expresion)auxiliar(actual.ChildNodes[0]));
            }

            else if (EstoyAca(actual, "EXPRESION"))
            {
                return auxiliar(actual.ChildNodes[0]);
            }
            else if (EstoyAca(actual, "EXPRESION_ARITMETICA"))
            {
                if (actual.ChildNodes.Count == 3)
                {
                    return new Operacion((Expresion)auxiliar(actual.ChildNodes[0]), (Expresion)auxiliar(actual.ChildNodes[2]), Operacion.getOperador(getLexema(actual, 1)));
                }
                else if (actual.ChildNodes.Count == 2)
                {
                    return new Operacion((Expresion)auxiliar(actual.ChildNodes[1]), Operacion.Operador.MENOS_UNARIO);
                }
            }
            else if (EstoyAca(actual, "EXPRESION_RELACIONAL"))
            {
                return new Operacion((Expresion)auxiliar(actual.ChildNodes[0]), (Expresion)auxiliar(actual.ChildNodes[2]), Operacion.getOperador(getLexema(actual, 1)));
            }
            else if (EstoyAca(actual, "EXPRESION_LOGICA"))
            {
                if (actual.ChildNodes.Count == 3)
                {
                    return new Operacion((Expresion)auxiliar(actual.ChildNodes[0]), (Expresion)auxiliar(actual.ChildNodes[2]), Operacion.getOperador(getLexema(actual, 1)));
                }
                else if (actual.ChildNodes.Count == 2)
                {
                    return new Operacion((Expresion)auxiliar(actual.ChildNodes[1]), Operacion.Operador.NOT);
                }
            }
            else if (EstoyAca(actual, "PRIMITIVO"))
            {

                if (EstoyAca(actual.ChildNodes[0], "numero"))
                {
                    double result = Convert.ToDouble(getLexema(actual, 0));
                    try
                    {
                        int result2 = Convert.ToInt32(getLexema(actual, 0));
                        return new Primitivo(result2);

                    }
                    catch (Exception)
                    {
                        return new Primitivo(result);
                    }
                }
                else if (EstoyAca(actual.ChildNodes[0], "cadena"))
                {
                    string aux = getLexema(actual, 0).ToString();
                    aux = aux.Replace("\\n", "\n");
                    aux = aux.Replace("\\t", "\t");
                    aux = aux.Replace("\\r", "\r");
                    aux = aux.Substring(1, aux.Length - 2);
                    return new Primitivo(aux);
                }
                else if (EstoyAca(actual.ChildNodes[0], "caracter"))
                {
                    try
                    {
                        string aux = getLexema(actual, 0);
                        char result = Convert.ToChar(aux.Substring(1, 1));
                        return new Primitivo(result);
                    }
                    catch (Exception)
                    {

                    }
                }
                else if (EstoyAca(actual.ChildNodes[0], "true"))
                {
                    return new Primitivo(true);
                }
                else if (EstoyAca(actual.ChildNodes[0], "false"))
                {
                    return new Primitivo(false);
                }
                else if (EstoyAca(actual.ChildNodes[0], "id"))
                {
                    return new Identificador(getLexema(actual, 0));
                }
            }
            else if (EstoyAca(actual, "TIPO"))
            {
                if (EstoyAca(actual.ChildNodes[0], "text"))
                {
                    return Simbolo.Tipos.STRING;
                }
                else if (EstoyAca(actual.ChildNodes[0], "bool"))
                {
                    return Tipos.BOOL;
                }
                else if (EstoyAca(actual.ChildNodes[0], "integer"))
                {
                    return Tipos.INT;
                }
                else if (EstoyAca(actual.ChildNodes[0], "double"))
                {
                    return Tipos.DOUBLE;
                }
                else if (EstoyAca(actual.ChildNodes[0], "char"))
                {
                    return Tipos.CHAR;
                }
                else if (EstoyAca(actual.ChildNodes[0], "date"))
                {
                    return Tipos.DATE;
                }
                else if (EstoyAca(actual.ChildNodes[0], "datetime"))
                {
                    return Tipos.DATETIME;
                }
                else
                {
                    return Tipos.OBJETO;
                }
            }
            return null;
        }

        

        static bool comparar(string a, string b)
        {
            return (a.Equals(b, System.StringComparison.InvariantCultureIgnoreCase));
        }



        static bool EstoyAca(ParseTreeNode nodo, string nombre)
        {
            return nodo.Term.Name.Equals(nombre, System.StringComparison.InvariantCultureIgnoreCase);
        }

        static string getLexema(ParseTreeNode nodo, int num)
        {
            return nodo.ChildNodes[num].Token.Text.Replace("@", "");
        }
    }

}