﻿using Irony.Parsing;

namespace BaseDeDatos.Analizadores
{
    class GramaticaP : Grammar
    {
        public GramaticaP() : base(caseSensitive: false)
        {
            #region ER
            NumberLiteral numero = TerminalFactory.CreateCSharpNumber("numero");          
           // var cadena = new StringLiteral("cadena", "\"", StringOptions.AllowsLineBreak);
            var cadena = new StringLiteral("cadena", "~", StringOptions.AllowsLineBreak);

            #endregion

            #region palabrasReservadas
            KeyTerm
            prValidar = ToTerm("\"validar\""),
            prLogin = ToTerm("\"login\""),
            prPassword = ToTerm("\"password\""),
            prPaquete = ToTerm("\"paquete\""),
            prFin = ToTerm("\"fin\""),
            prUsql = ToTerm("\"usql\""),
            prInstruccion = ToTerm("\"instruccion\""),
            prReporte = ToTerm("\"reporte\"");

            

           // MarkReservedWords("");

            #endregion palabrasReservadas

            #region Operadores y simbolos
            Terminal ptcoma = ToTerm(";"),
                coma = ToTerm(","),
                punto = ToTerm("."),
                dospuntos = ToTerm(":"),
        
                corizq = ToTerm("["),
                corder = ToTerm("]");
            #endregion

            #region No terminales
            NonTerminal S = new NonTerminal("S"),
             TIPO = new NonTerminal("TIPO"),
             EXPRESION = new NonTerminal("EXPRESION"),
             OPMAT = new NonTerminal("OPMAT"),
             SENTENCIA = new NonTerminal("SENTENCIA"),
             SENTENCIA_LISTA = new NonTerminal("SENTENCIA_LISTA"),
             ASIGNACION = new NonTerminal("ASIGNACION"),
             VISIBILIDAD = new NonTerminal("VISIBILIDAD");
            #endregion

            var PROGRAMA = new NonTerminal("PROGRAMA");
            var PAQUETE = new NonTerminal("PAQUETE");
            var PAQUETE_LISTA = new NonTerminal("PAQUETE_LISTA");
            var PAQUETE_LOGIN = new NonTerminal("PAQUETE_LOGIN");
            var PAQUETE_FIN = new NonTerminal("PAQUETE_FIN");
            var PAQUETE_REPORTE = new NonTerminal("PAQUETE_REPORTE");
            var PAQUETE_SQL = new NonTerminal("PAQUETE_SQL");


            #region inicio
            PROGRAMA.Rule = PAQUETE_LISTA;
            #endregion fin inicio

            PAQUETE_LISTA.Rule = MakePlusRule(PAQUETE_LISTA, PAQUETE);

            PAQUETE.Rule = PAQUETE_LOGIN | PAQUETE_FIN | PAQUETE_REPORTE | PAQUETE_SQL;

            PAQUETE_LOGIN.Rule = corizq + prValidar + dospuntos + numero + coma
                + prLogin + dospuntos + corizq
                + prInstruccion + dospuntos + cadena
                + corder + corder ;

            PAQUETE_FIN.Rule = corizq + prPaquete + dospuntos + prFin + corder;

            PAQUETE_SQL.Rule = corizq + prPaquete + dospuntos + prUsql + coma 
                + prInstruccion + dospuntos + cadena + corder;

            PAQUETE_REPORTE.Rule = corizq +  prPaquete + dospuntos + prReporte + coma
                + prInstruccion + dospuntos + cadena + corder;


            #region Preferencias
            this.Root = PROGRAMA;
            this.MarkTransient(PAQUETE );

            //---------------------> Eliminacion de caracters, no terminales
            this.MarkPunctuation( ":", ",", "[", "]");
            this.MarkPunctuation("repetir", "\"validar\"", "\"login\"", "\"password\"", "\"paquete\"", "\"fin\"", "\"usql\"", "\"instruccion\"", "\"reporte\"");
           
            #endregion


        }
    }
}
  

