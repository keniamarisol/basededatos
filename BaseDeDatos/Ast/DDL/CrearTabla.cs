﻿using BaseDeDatos.SistemaDeArchivos;
using Irony.Parsing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseDeDatos.Ast.DDL
{
    public class CrearTabla : Instruccion
    {

        String  nombre;
        public List<Campo> campos;

        public CrearTabla(string nombre, List<Campo> campos)
        {
            this.nombre = nombre;
            this.campos = campos;
        }

        public object ejecutar(Entorno ent, AST arbol)
        {
            Tabla tabla = new Tabla(nombre, campos);
            SistemaDeBaseDeDatos.crearTabla(tabla);
            return null;
        }



        public string getC3D()
        {
            throw new NotImplementedException();
        }
    }
}
