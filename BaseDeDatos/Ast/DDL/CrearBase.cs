﻿using BaseDeDatos.SistemaDeArchivos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseDeDatos.Ast.DDL
{
    public class CrearBase : Instruccion
    {
        String nombre;

        public CrearBase(String nombre)
        {
            this.nombre = nombre.ToLower();
        }

        public object ejecutar(Entorno ent, AST arbol)
        {
            SistemaDeBaseDeDatos.crearBD(nombre, 1);
            return null;
        }

        public string getC3D()
        {
            throw new NotImplementedException();
        }
    }
}

