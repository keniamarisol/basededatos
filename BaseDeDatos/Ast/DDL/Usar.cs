﻿using BaseDeDatos.SistemaDeArchivos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseDeDatos.Ast.DDL
{
    class Usar : Instruccion
    {
        String nombre;

        public Usar(string nombre)
        {
            this.nombre = nombre;
        }

        public object ejecutar(Entorno ent, AST arbol)
        {
            SistemaDeBaseDeDatos.usarBD(nombre);
            return null;
        }

        public string getC3D()
        {
            throw new NotImplementedException();
        }
    }
}
