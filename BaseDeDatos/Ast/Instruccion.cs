﻿using BaseDeDatos.Ast;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseDeDatos.Ast
{
     public interface Instruccion : NodoAST
    {

        object ejecutar(Entorno ent, AST arbol);
    }
}
