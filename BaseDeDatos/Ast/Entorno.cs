﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseDeDatos.Ast
{
    public class Entorno
    {

        private Hashtable tabla;

        private Entorno anterior;

        public Entorno(Entorno anterior)
        {
            Tabla = new Hashtable();
            this.Anterior = anterior;
        }

        public void agregar(string id, Simbolo simbolo)
        {
            Tabla.Add(id, simbolo);
        }

        
        public bool existe(string id)
        {
            for (Entorno e = this; e != null; e = e.Anterior)
            {
                if (e.Tabla.Contains(id))
                {
                    return true;
                }
            }
            return false;
        }

        public bool existeEnActual(string id)
        {
            Simbolo encontrado = (Simbolo)(Tabla[id]);
            return encontrado != null;
        }
        public Simbolo get(string id)
        {
            for (Entorno e = this; e != null; e = e.Anterior)
            {
                Simbolo encontrado = (Simbolo)(e.Tabla[id]);
                if (encontrado != null)
                {
                    return encontrado;
                }
            }
            Console.WriteLine("El simbolo \"" + id + "\" no ha sido declarado en el Ast actual ni en alguno externo");
            return null;
        }

        public void reemplazar(string id, Simbolo nuevoValor)
        {
            for (Entorno e = this; e != null; e = e.Anterior)
            {
                Simbolo encontrado = (Simbolo)(e.Tabla[id]);
                if (encontrado != null)
                {
                    Tabla[id] = nuevoValor;
                }
            }
            Console.WriteLine("El simbolo \"" + id + "\" no ha sido declarado en el Ast actual ni en alguno externo");

        }

        public Hashtable Tabla
        {
            get
            {
                return tabla;
            }

            set
            {
                tabla = value;
            }
        }

        public Entorno Anterior
        {
            get
            {
                return anterior;
            }

            set
            {
                anterior = value;
            }
        }

    }
}

