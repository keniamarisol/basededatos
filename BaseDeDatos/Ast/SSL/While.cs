﻿using BaseDeDatos.Analizadores;
using BaseDeDatos.Ast;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//KENIA MARISOL ZEPEDA LÓPEZ 201212623

namespace BaseDeDatos.Ast.SSL

{
    public class While : Condicional, Instruccion
    {
        public While(LinkedList<NodoAST> Instrucciones, Expresion Condicion) :
                base(Condicion, Instrucciones)
        {

        }

        public object ejecutar(Entorno ent, AST arbol)
        {

            Program.getGUI().contador_break = 1;
            siguiente:
            while ((bool)Condicion.getValorImplicito(ent, arbol))
            {
                Entorno local = new Entorno(ent);
                foreach (Instruccion ins in Instrucciones)
                {
                    object result = ins.ejecutar(local, arbol);
                    if (result != null)
                    {
                        return result;
                    }
                    if (ins is Break)
                    {
                        Program.getGUI().contador_break = 0;
                        return null;
                    }
                    if (ins is Continue)
                    {
                        goto siguiente;
                    }
                }
            }

            Program.getGUI().contador_break = 0;
            return null;
        }

        public string getC3D()
        {
            throw new NotImplementedException();
        }

        
    }
}
