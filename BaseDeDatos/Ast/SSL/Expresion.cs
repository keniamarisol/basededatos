﻿using BaseDeDatos.Ast;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static BaseDeDatos.Ast.Simbolo;

namespace BaseDeDatos.Ast.SSL
{
    public interface Expresion : NodoAST
    {

        Tipos getTipo(Entorno ent, AST arbol);


        object getValorImplicito(Entorno ent, AST arbol);
    }
}
