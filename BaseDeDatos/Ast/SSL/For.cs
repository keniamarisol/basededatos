﻿using System;
using System.Collections.Generic;

namespace BaseDeDatos.Ast.SSL
{
    public class For : Instruccion
    {

        LinkedList<NodoAST> Instrucciones;
        Expresion Condicion;
        Declaracion Declaracion;
        Asignacion ValorInicial;
        String IdVariable;
        Incremento Incre;

        public For(LinkedList<NodoAST> Instrucciones, Declaracion Declaracion, String IdVariable, Expresion Condicion, Incremento Incre)
        {
            this.Instrucciones = Instrucciones;
            this.Condicion = Condicion;
            this.Declaracion = Declaracion;
            this.IdVariable = IdVariable;
            this.Incre = Incre;
        }

        public object ejecutar(Entorno entorno, AST arbol)
        {
            Program.getGUI().contador_break = 1;
            Entorno paraEntorno = new Entorno(entorno);
            Declaracion.ejecutar(paraEntorno, arbol);
            //  ValorInicial.ejecutar(paraEntorno, arbol);


            Object val = (bool)Condicion.getValorImplicito(paraEntorno, arbol);
            if (val is bool)
            {
                int t = 0;
                while ((bool)Condicion.getValorImplicito(paraEntorno, arbol))
                {
                    Entorno ent = new Entorno(paraEntorno);
                    Object obj;
                    Entorno local = new Entorno(ent);
                    foreach (Instruccion ins in Instrucciones)
                    {
                        object result = ins.ejecutar(local, arbol);
                        if (result != null)
                        {
                            return result;
                        }
                        if (ins is Break)
                        {
                            Program.getGUI().contador_break = 0;
                            return null;
                        }
                       
                    }


                    Simbolo var = paraEntorno.get(IdVariable);
                    if (var != null && var.Valor is int)
                    {
                        if (this.Incre == Incremento.INCREMENTO)
                        {
                            var.Valor = (int)var.Valor + 1;
                        }
                        else
                        {
                            var.Valor = (int)var.Valor - 1;
                        }

                    }
                    else
                    {
                        Program.getGUI().appendSalida("Error en la instrucción PARA, la variable no existe ");
                        return null;
                    }
                    t++;
                    if (t == 1000)
                    {
                        Program.getGUI().appendSalida("Error en la instrucción PARA, se quedó enciclado ");
                        return null;
                    }
                }
            }
            else
            {
                Program.getGUI().appendSalida("Error en la instrucción PARA, la condición no es válida ");
                return null;
            }

            Program.getGUI().contador_break = 0;
            return null;
        }

        public enum Incremento
        {
            INCREMENTO,
            DECREMENTO
        }

        public string getC3D()
        {
            throw new NotImplementedException();
        }
    }

}
