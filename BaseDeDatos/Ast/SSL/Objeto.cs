﻿namespace BaseDeDatos.Ast
{
    public class Objeto : Simbolo
    {
        private string idStructGenerador;
        private Entorno atributos;

        public Objeto(string claseGeneradora, Entorno atributos) : base(Tipos.OBJETO, claseGeneradora)
        {
            this.idStructGenerador = claseGeneradora;
            this.atributos = atributos;
        }

        /**
         * @return the claseGeneradora
         */
        public string getClaseGeneradora()
        {
            return idStructGenerador;
        }

        /**
         * @return the atributos
         */
        public Entorno getAtributos()
        {
            return atributos;
        }

    }
    
    
}
