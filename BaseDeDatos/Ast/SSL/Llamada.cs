﻿using System;
using System.Collections;
using System.Collections.Generic;
using static BaseDeDatos.Ast.Simbolo;

namespace BaseDeDatos.Ast.SSL
{
    class Llamada : Expresion
    {

        private string identificador;

        private LinkedList<Expresion> valores;


        public Llamada(string identificador)
        {
            this.identificador = identificador;
            this.valores = new LinkedList<Expresion>();
        }

        public Llamada(string identificador, LinkedList<Expresion> valores)
        {
            this.identificador = identificador;
            this.valores = valores;
        }

        public Tipos getTipo(Entorno ent, AST arbol)
        {
            object valor = this.getValorImplicito(ent, arbol);
            if (valor is bool)
            {
                return Tipos.BOOL;
            }
            else if (valor is string)
            {
                return Tipos.STRING;
            }
            else if (valor is char)
            {
                return Tipos.CHAR;
            }
            else if (valor is int)
            {
                return Tipos.INT;
            }
            else if (valor is double)
            {
                return Tipos.DOUBLE;
            }
            else if (valor is Objeto)
            {
                return Tipos.OBJETO;
            }
            else
            {
                return Tipos.NULL;
            }
        }

        public object getValorImplicito(Entorno ent, AST arbol)
        {
            if (ent.existe(identificador))
            {
                Funcion funcion = (Funcion)ent.get(identificador);
                Entorno local = new Entorno(ent);
               
                if (verificarParametros(valores, funcion.ListaParametros, local, arbol))
                {

                    //aqui va el retorno de lo que tenes que hacer
                    return funcion.ejecutar(local, arbol);

                }
            }
            else
            {
                Program.getGUI().appendSalida("La funcion" + identificador + " no ha sido declarada");
            }
            return null;
        }


        bool verificarParametros(LinkedList<Expresion> valores, LinkedList<Simbolo> parametros, Entorno ent, AST arbol)
        {
            IList param = new List<Simbolo>(parametros);
            IList vals = new List<Expresion>(valores);
            if (vals.Count == param.Count)
            {

                Simbolo sim_aux;
                string id_aux;
                Tipos tipoPar_aux;
                Tipos tipoVal_aux;
                Expresion exp_aux;
                object val_aux;

                for (int i = 0; i < parametros.Count; i++)
                {
                    sim_aux = (Simbolo)param[i];
                    id_aux = sim_aux.Identificador;
                    tipoPar_aux = sim_aux.Tipo;

                    exp_aux = (Expresion)vals[i];
                    tipoVal_aux = exp_aux.getTipo(ent, arbol);
                    val_aux = exp_aux.getValorImplicito(ent, arbol);
                    if (tipoPar_aux == tipoVal_aux)
                    {
                        ent.agregar(id_aux, new Simbolo(tipoPar_aux, id_aux, val_aux));
                    }
                    else
                    {
                        Program.getGUI().appendSalida("Llamada a: " + identificador + ", El tipo de los parametros no coinciden con "
                                + "el valor correspondiente utilizado en la llamada, Parametro:" + id_aux);
                        return false;
                    }
                }
                return true;
            }
            else
            {
                Program.getGUI().appendSalida("Llamada a: " + identificador + "La cantidad de parametros no coincide");
            }
            return false;
        }

        public string getC3D()
        {
            throw new NotImplementedException();
        }

    }
}
