﻿using System;
using System.Collections.Generic;
using static BaseDeDatos.Ast.Simbolo;

namespace BaseDeDatos.Ast.SSL
{
    public class Asignacion : Instruccion
    {
        private string idObjeto;
        private string idAtributo;
        private LinkedList<Simbolo> variables;
        private bool accesoObjeto;
        private Expresion valor;

 
        public Asignacion(LinkedList<Simbolo> variables, Expresion valor)
        {
            this.variables = variables;
            this.valor = valor;
            this.accesoObjeto = false;
            if (valor == null)
            {
                Program.getGUI().appendSalida("El valor por asignar es nulo o no valido.");
            }

        }


        public Asignacion(string idObjeto, string idAtributo, Expresion valor)
        {
            this.valor = valor;
            this.idAtributo = idAtributo;
            this.idObjeto = idObjeto;
            accesoObjeto = true;
            variables = new LinkedList<Simbolo>();
            if (valor == null)
            {
                Program.getGUI().appendSalida("El valor por asignar es nulo o no valido.");
            }
        }
        public object ejecutar(Entorno ent, AST arbol)
        {
            object val = valor.getValorImplicito(ent, arbol);
            if (accesoObjeto)
            {
                Entorno atributosSinModificar = ((Objeto)ent.get(idObjeto)).getAtributos();
                Entorno atributosNuevos = new Entorno(null);
                foreach (Simbolo atributo in atributosSinModificar.Tabla.Values)
                {
                    if (!atributo.Identificador.Equals(idAtributo, StringComparison.InvariantCultureIgnoreCase))
                    {
                        atributosNuevos.agregar(atributo.Identificador, atributo);
                    }
                }
                atributosNuevos.agregar(idAtributo, new Simbolo(atributosSinModificar.get(idAtributo).Tipo, idAtributo, val));
                ent.reemplazar(idObjeto, new Objeto(idObjeto, atributosNuevos));
            }
            else
            {
                foreach (Simbolo variable in variables)
                {
                    string nombreVariable = variable.Identificador;
                    Tipos tipoVal = valor.getTipo(ent, arbol);
                    Tipos tipoVar = ent.get(nombreVariable).Tipo;
                    if (valor is Llamada)
                    {
                        Llamada ob = (Llamada)valor;
                        if (ob.getTipo(ent, arbol) == Tipos.NULL)
                        {
                            Program.getGUI().appendSalida("No es posible asignarle valor a: " + idAtributo + ","
                                           + "Debido a que las funciones de tipo void (Procedimientos) "
                                           + "No devuelven un valor ");
                            return null;
                        }
                    }
                    if (tipoVal == tipoVar)
                    {
                        if (val is Objeto)
                        {
                            ent.reemplazar(nombreVariable, (Objeto)val);
                        }
                        else
                        {
                            ent.get(nombreVariable).Valor = val;
                        }
                    }
                    else
                    {
                        Program.getGUI().appendSalida("Error de tipos"
                                + ", se intenta setear un valor a la variable "
                                + nombreVariable + " diferente al que fue declarado");
                        return null;
                    }

                }
            }

            return null;
        }

        public string getC3D()
        {
            throw new NotImplementedException();
        }
    }
    
}
