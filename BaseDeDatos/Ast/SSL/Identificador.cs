﻿using BaseDeDatos.Ast;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static BaseDeDatos.Ast.Simbolo;

namespace BaseDeDatos.Ast.SSL
{
    public class Identificador : Expresion
    {
        private string identificador;

        public Identificador(string identificador)
        {
            this.identificador = identificador;
        }

        public Tipos getTipo(Entorno ent, AST arbol)
        {
            object valor = getValorImplicito(ent, arbol);
            if (valor is bool)
            {
                return Tipos.BOOL;
            }
            else if (valor is string)
            {
                return Tipos.STRING;
            }
            else if (valor is char)
            {
                return Tipos.CHAR;
            }
            else if (valor is int)
            {
                return Tipos.INT;
            }
            else if (valor is double)
            {
                return Tipos.DOUBLE;
            }
            else if (valor is Objeto)
            {
                return Tipos.OBJETO;
            }
            else
            {
                return Tipos.NULL;
            }
        }

        public object getValorImplicito(Entorno ent, AST arbol)
        {
            Simbolo sim = ent.get(identificador);
            return sim.Tipo == Tipos.OBJETO ? sim : sim.Valor;
        }

        public string getC3D()
        {
            throw new NotImplementedException();
        }
    }
    
}
