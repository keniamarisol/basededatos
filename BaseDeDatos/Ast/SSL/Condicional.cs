﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseDeDatos.Ast.SSL
{
    public class Condicional
    {
        private LinkedList<NodoAST> instrucciones;

        private Expresion condicion;

        

        public Condicional(Expresion Condicion, LinkedList<NodoAST> Instrucciones)
        {
            this.Instrucciones = Instrucciones;
            this.Condicion = Condicion;
        }

       

        public LinkedList<NodoAST> Instrucciones
        {
            get
            {
                return instrucciones;
            }

            set
            {
                instrucciones = value;
            }
        }

       

        public Expresion Condicion
        {
            get
            {
                return condicion;
            }

            set
            {
                condicion = value;
            }
        }

    
}
}
