﻿using BaseDeDatos.Ast;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//KENIA MARISOL ZEPEDA LÓPEZ 201212623

namespace BaseDeDatos.Ast.SSL
{
    public class If : Condicional, Instruccion
    {
        
        private LinkedList<NodoAST> instruccionesElse;

        public If(Expresion condicion, LinkedList<NodoAST> instrucciones, LinkedList<NodoAST> InstruccionesElse)
        : base(condicion, instrucciones)
        {
            this.InstruccionesElse = InstruccionesElse;
        }


        public object ejecutar(Entorno ent, AST arbol)
        {
            if ((bool)(Condicion.getValorImplicito(ent, arbol)))
            {
                // Si la condicion del if se cumple, entonces ejecuto las instrucciones.
                Entorno local = new Entorno(ent);
                foreach (NodoAST nodo in Instrucciones)
                {
                    if (nodo is Continue) {
                        return nodo;
                    }

                    if (nodo is Break)
                    {
                        Program.getGUI().contador_break = 0;
                        return nodo;
                    }

                    if (nodo is Instruccion)
                    {
                        Instruccion ins = (Instruccion)nodo;
                        object result = ins.ejecutar(local, arbol);
                        if (result != null)
                        {
                            return result;
                        }
                    }
                    else if (nodo is Expresion)
                    {
                        Expresion expr = (Expresion)nodo;
                        return expr.getValorImplicito(local, arbol);
                    }
                }
            }
            else
            {
                Entorno local = new Entorno(ent);
                foreach (NodoAST nodo in InstruccionesElse)
                {
                   
                    if (nodo is Instruccion)
                    {
                        Instruccion ins = (Instruccion)nodo;
                        object result = ins.ejecutar(local, arbol);
                        if (result != null)
                        {
                            return result;
                        }
                    }
                    else if (nodo is Expresion)
                    {
                        Expresion expr = (Expresion)nodo;
                        return expr.getValorImplicito(local, arbol);
                    }
                }
            }
            return null;
        }

     

        public LinkedList<NodoAST> InstruccionesElse
        {
            get
            {
                return instruccionesElse;
            }

            set
            {
                instruccionesElse = value;
            }
        }

        public string getC3D()
        {
            throw new NotImplementedException();
        }
    }

}
