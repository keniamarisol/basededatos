﻿using System;
using static BaseDeDatos.Ast.Simbolo;

namespace BaseDeDatos.Ast.SSL
{
    class Return : Expresion
    {
        private bool retornoVoid;
        private Expresion valorDeRetorno;

        public Return(Expresion valorDeRetorno)
        {
            this.valorDeRetorno = valorDeRetorno;
            retornoVoid = false;
        }

        public Return()
        {
            retornoVoid = true;
        }

        public bool isRetornoVoid()
        {
            return retornoVoid;
        }

        public Tipos getTipo(Entorno ent, AST arbol)
        {
            return valorDeRetorno.getTipo(ent, arbol);
        }


        public object getValorImplicito(Entorno ent, AST arbol)
        {
            return valorDeRetorno.getValorImplicito(ent, arbol);
        }

        public string getC3D()
        {
            throw new NotImplementedException();
        }

    }
}
