﻿using System;
using static BaseDeDatos.Ast.Simbolo;

namespace BaseDeDatos.Ast.SSL
{
    public class Primitivo : Expresion
    {
   
        private object valor;

        public Primitivo(object valor)
        {
            this.valor = valor;
        }
        public Tipos getTipo(Entorno ent, AST arbol)
        {
            object valor = this.getValorImplicito(ent, arbol);
            if (valor is bool)
            {
                return Tipos.BOOL;
            }
            else if (valor is string)
            {
                return Tipos.STRING;
            }
            else if (valor is char)
            {
                return Tipos.CHAR;
            }
            else if (valor is int)
            {
                return Tipos.INT;
            }
            else if (valor is double)
            {
                return Tipos.DOUBLE;
            }
            else
            {
                return Tipos.OBJETO;
            }
        }

        public object getValorImplicito(Entorno ent, AST arbol)
        {
            return valor;
        }

        public string getC3D()
        {
            throw new NotImplementedException();
        }

    }
  
}
