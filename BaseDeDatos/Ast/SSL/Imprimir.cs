﻿using BaseDeDatos.Ast;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseDeDatos.Ast.SSL
{
    public class Imprimir : Instruccion
    {

        private Expresion expresion;

        public Imprimir(Expresion expresion)
        {
            this.expresion = expresion;
        }


        public object ejecutar(Entorno ent, AST arbol)
        {
            object ob = expresion.getValorImplicito(ent, arbol);
            if (ob != null)
            {
                Program.getGUI().appendSalida(ob.ToString());
            }
            else
            {
                Program.getGUI().appendSalida("Se intentó imprimir una expresion nula");
            }
            return null;
        }

        public string getC3D()
        {
            throw new NotImplementedException();
        }
    }
}