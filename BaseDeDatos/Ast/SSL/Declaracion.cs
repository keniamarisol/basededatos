﻿using BaseDeDatos.Ast;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static BaseDeDatos.Ast.Simbolo;

namespace BaseDeDatos.Ast.SSL
{
    public class Declaracion : Instruccion
    {

        private Expresion valorInicilizacion;

        private LinkedList<Simbolo> variables;

        bool isInicializacion()
        {
            return this.valorInicilizacion != null;
        }


        public Declaracion(Tipos tipo, LinkedList<Simbolo> variables, Expresion valor)
        {
            foreach (Simbolo variable in variables)
            {
                variable.Tipo = tipo;
            }
            this.variables = variables;
            this.valorInicilizacion = valor;
        }



        public Declaracion(Tipos tipo, Simbolo variable)
        {
            variable.Tipo = tipo;
            LinkedList<Simbolo> variables = new LinkedList<Simbolo>();
            variables.AddLast(variable);
            this.variables = variables;
            this.valorInicilizacion = null;
        }

        public object ejecutar(Entorno ent, AST arbol)
        {
            foreach (Simbolo variable in variables)
            {
                string nombreVariable = variable.Identificador;
                if (isInicializacion())
                {
                    Tipos tipoVal = valorInicilizacion.getTipo(ent, arbol);
                    Tipos tipoVar = variable.Tipo;
                    if (ent.existeEnActual(nombreVariable))
                    {
                        Program.getGUI().appendSalida("Se intento declarar " + nombreVariable
                                + "una variable ya existente en el Ast actual");
                    }
                    else
                    {
                        if (tipoVal == tipoVar)
                        {
                            object val = valorInicilizacion.getValorImplicito(ent, arbol);
                            variable.Valor = val;
                            ent.agregar(nombreVariable, variable);
                        }
                        else
                        {
                            Program.getGUI().appendSalida("Error de tipos"
                                    + ", se intenta setear un valor a la variable "
                                    + nombreVariable + " diferente al que fue declarado");
                        }
                    }
                }
                else
                {
                    if (ent.existeEnActual(nombreVariable))
                    {
                        Program.getGUI().appendSalida("Se intento declarar "
                                + "una variable ya existente en el Ast actual");
                    }
                    else
                    {
                        ent.agregar(nombreVariable, variable);
                    }
                }

            }

            return null;
        }

        public string getC3D()
        {
            throw new NotImplementedException();
        }
    }
}
