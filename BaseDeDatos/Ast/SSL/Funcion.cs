﻿using System;
using System.Collections.Generic;

namespace BaseDeDatos.Ast.SSL
{
    public class Funcion : Simbolo, Instruccion
    {
        private string tipoStruct;
        private LinkedList<NodoAST> instrucciones;

        public Funcion(Tipos tipo, string identificador, LinkedList<Simbolo> listaParametros, LinkedList<NodoAST> instrucciones)
            : base(identificador, tipo, listaParametros)
        {
            TipoStruct = string.Empty;
            this.Instrucciones = instrucciones;
        }

        public Funcion(string tipoStruct, string identificador, LinkedList<Simbolo> listaParametros, LinkedList<NodoAST> instrucciones)
             : base(identificador, Tipos.OBJETO, listaParametros)
        {
            this.TipoStruct = tipoStruct;
            this.Instrucciones = instrucciones;
        }


        public object ejecutar(Entorno ent, AST arbol)
        {
            foreach (NodoAST nodo in Instrucciones)
            {
                if (nodo is Instruccion)
                {
                    Instruccion ins = (Instruccion)nodo;
                    object result = ins.ejecutar(ent, arbol);
                    if (result != null)
                    {
                        if (verificarTipo(this.Tipo, result))
                        {
                            return result;
                        }
                        else
                        {
                            Program.getGUI().appendSalida("EL tipo del retorno no es el declarado en la funcion");
                            return null;
                        }

                    }
                }
                else if (nodo is Expresion)
                {
                    Expresion expr = (Expresion)nodo;
                    object result = expr.getValorImplicito(ent, arbol);
                    if (result != null)
                    {
                        if (expr.getTipo(ent, arbol) == this.Tipo)
                        {
                            return result;
                        }
                        else
                        {
                            Program.getGUI().appendSalida("EL tipo del retorno no es el declarado en la funcion");
                            return null;
                        }

                    }
                }
            }
            return null;
        }

        private bool verificarTipo(object tipo, object result)
        {
            throw new NotImplementedException();
        }

        private bool verificarTipo(Tipos tipo, object result)
        {
            if (tipo == Tipos.INT && result is int)
            {
                return true;
            }
            if (tipo == Tipos.STRING && result is string)
            {
                return true;
            }
            else if (tipo == Tipos.CHAR && result is char)
            {
                return true;
            }
            else if (tipo == Tipos.DOUBLE && result is Double)
            {
                return true;
            }
            else if (tipo == Tipos.BOOL && result is bool)
            {
                return true;
            }
            else if (tipo == Tipos.OBJETO && result is Objeto)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

    

        public LinkedList<NodoAST> Instrucciones
        {
            get
            {
                return instrucciones;
            }

            set
            {
                instrucciones = value;
            }
        }

  

        public string TipoStruct
        {
            get
            {
                return tipoStruct;
            }

            set
            {
                tipoStruct = value;
            }
        }

        public string getC3D()
        {
            throw new NotImplementedException();
        }
    }
}
