﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseDeDatos.Ast
{
    
        public class Simbolo
        {

            public enum Tipos
            {
                STRING,
                INT,
                DOUBLE,
                CHAR,
                BOOL,
                DATE,
                DATETIME,
                NULL,
                OBJETO
            }


            private string identificador;

            private object valor;

            private Tipos tipo;

            private LinkedList<Simbolo> listaParametros;

            private bool funcion;

            public Simbolo(string Identificador)
            {
                this.Identificador = Identificador;
                this.Funcion = false;
            }


            public Simbolo(Tipos Tipo, string Identificador)
            {
                this.Tipo = Tipo;
                this.Identificador = Identificador;
                Funcion = false;
            }

            public Simbolo(Tipos Tipo, string Identificador, object Valor)
            {
                this.Tipo = Tipo;
                this.Identificador = Identificador;
                this.Valor = Valor;
                Funcion = false;
            }

           

            public Simbolo(string Identificador, Tipos Tipo, LinkedList<Simbolo> ListaParametros)
            {
                this.Identificador = Identificador;
                this.Tipo = Tipo;
                this.ListaParametros = ListaParametros;
                Funcion = true;
            }

           
            public string Identificador
            {
                get
                {
                    return identificador;
                }

                set
                {
                    identificador = value;
                }
            }

 
            public object Valor
            {
                get
                {
                    return valor;
                }

                set
                {
                    valor = value;
                }
            }

 
            public Tipos Tipo
            {
                get
                {
                    return tipo;
                }

                set
                {
                    tipo = value;
                }
            }

            public LinkedList<Simbolo> ListaParametros
            {
                get
                {
                    return listaParametros;
                }

                set
                {
                    listaParametros = value;
                }
            }

            public bool Funcion
            {
                get
                {
                    return funcion;
                }

                set
                {
                    funcion = value;
                }
            }
        }
    }

