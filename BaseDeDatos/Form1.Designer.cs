﻿namespace BaseDeDatos
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.textEntrada = new System.Windows.Forms.RichTextBox();
            this.btnAnalizar = new System.Windows.Forms.Button();
            this.textConsola = new System.Windows.Forms.RichTextBox();
            this.btnSendInfoSockets = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textEntrada
            // 
            this.textEntrada.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEntrada.Location = new System.Drawing.Point(21, 41);
            this.textEntrada.Name = "textEntrada";
            this.textEntrada.Size = new System.Drawing.Size(1000, 311);
            this.textEntrada.TabIndex = 0;
            this.textEntrada.Text = "";
            // 
            // btnAnalizar
            // 
            this.btnAnalizar.Location = new System.Drawing.Point(21, 12);
            this.btnAnalizar.Name = "btnAnalizar";
            this.btnAnalizar.Size = new System.Drawing.Size(65, 23);
            this.btnAnalizar.TabIndex = 1;
            this.btnAnalizar.Text = "Analizar";
            this.btnAnalizar.UseVisualStyleBackColor = true;
            this.btnAnalizar.Click += new System.EventHandler(this.btnAnalizar_Click);
            // 
            // textConsola
            // 
            this.textConsola.BackColor = System.Drawing.SystemColors.InfoText;
            this.textConsola.Font = new System.Drawing.Font("OCR A Extended", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textConsola.ForeColor = System.Drawing.SystemColors.Window;
            this.textConsola.Location = new System.Drawing.Point(21, 358);
            this.textConsola.Name = "textConsola";
            this.textConsola.Size = new System.Drawing.Size(1000, 325);
            this.textConsola.TabIndex = 2;
            this.textConsola.Text = "";
            // 
            // btnSendInfoSockets
            // 
            this.btnSendInfoSockets.Location = new System.Drawing.Point(92, 12);
            this.btnSendInfoSockets.Name = "btnSendInfoSockets";
            this.btnSendInfoSockets.Size = new System.Drawing.Size(75, 23);
            this.btnSendInfoSockets.TabIndex = 3;
            this.btnSendInfoSockets.Text = "Sockets";
            this.btnSendInfoSockets.UseVisualStyleBackColor = true;
            this.btnSendInfoSockets.Click += new System.EventHandler(this.btnSendInfoSockets_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1084, 749);
            this.Controls.Add(this.btnSendInfoSockets);
            this.Controls.Add(this.textConsola);
            this.Controls.Add(this.btnAnalizar);
            this.Controls.Add(this.textEntrada);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox textEntrada;
        private System.Windows.Forms.Button btnAnalizar;
        private System.Windows.Forms.Button btnSendInfoSockets;
        public System.Windows.Forms.RichTextBox textConsola;
    }
}

